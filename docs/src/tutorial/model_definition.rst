Model definition
================


:py:class:`~sigcorr.models.model.AbstractModel` in |project| consists of several required components:

* model parameters + ``init()`` method for them
* method for generating background samples
* background model, its derivatives, starting point for the optimization
* signal + background model, its derivatives, starting point for the optimization

You can find a fully developed example of the Hyy model on gitlab: :src:`sigcorr/models/hyy.py`. In this part of the tutorial
we will learn how to build this model from scratch.

We start by defining an empty class ``HyyTutorial`` for the model. If you installed |project| in the development mode,
then we suggest to create the new model as part of the :src:`sigcorr/models` package. The filename doesn't matter. Let's name it ``hyy_tutorial.py``, for example.

.. code::

    from sigcorr.models.model import AbstractModel


    class HyyTutorial(AbstractModel):
        def __init__(self, xs):
            self.xs = xs


Background generation
---------------------


Let's first define methods that are required for the Monte Carlo generation of background samples (background toys).
For the tutorial, the background model has an exponential shape with
two free parameters ``n_bg`` and ``xscale``:

.. code::

    import jax.numpy as jnp
    from sigcorr.tools.utils import jax_jit_method
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        @jax_jit_method
        def expected_b(self, n_bg, xscale):
            # shift by 100 GeV is applied to set the mass range around the real Higgs mass
            return n_bg*jnp.exp(-(self.xs-100)*xscale)

.. NOTE::
    We use `google/jax <https://github.com/google/jax>`_ for just-in-time compilation of the model functions.
    They will be evaluated a huge number of times, so we give it a slight boost. To enable JAX we should use
    the wrapper around numpy (:py:mod:`jax.numpy`) and scipy (:py:mod:`jax.scipy`), that we do via alias ``jnp`` and ``jsp``,
    and also mark functions that need to be JIT-compiled with :py:func:`jax.jit`. In |project| we created a very simple decorator
    :py:func:`~sigcorr.tools.utils.jax_jit_method` with a reduced functionality, that allows to denote methods of a class
    as requiring JIT-compilation.

The method :py:meth:`~sigcorr.models.model.AbstractModel.get_bg_samples` is responsible for generating background toys.
For the background (and the signal) we assume that the statistical fluctuations in each bin are uncorrelated with the other bins
and the errors are Gaussian
with known standard deviation ``BG_NOISE_STD``. For generating toys we also set the free parameters of the background model with
``n_bg = B0`` and ``xscale = self.BG_XSCALE``:

.. code::

    import scipy as sp
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = ["B0", "BG_XSCALE", "BG_NOISE_STD"]

        def __init__(self, xs):
            self.B0 = 10
            self.BG_XSCALE = 0.033
            self.BG_NOISE_STD = 0.3
            self.xs = xs

        def get_bg_samples(self, num_samples):
            return self.expected_b(self.B0, self.BG_XSCALE) + \
                   sp.stats.norm(scale=self.BG_NOISE_STD).rvs((num_samples, self.xs.shape[0]))

        @jax_jit_method
        def expected_b(self, n_bg, xscale):
            ...

.. NOTE::
    It is a good practice to store parameters as instance properties and to mention them in the ``PARAMS`` list.
    The parameters then will be stored as attributes in the output HDF5 file together with
    the background toys and fits.


Background-only fits
--------------------

For the fits of the background model to the toys (b-fits) we need to define:

    * initial values for the parameters ``self.b_guess``
    * "minus log-likelihood" (from now on we call it just "log-likelihood") function
      :py:meth:`~sigcorr.models.model.AbstractModel.b_minus_loglike`
    * derivatives of the log-likelihood: the vector gradient (:py:meth:`~sigcorr.models.model.AbstractModel.b_minus_loglike_grad`)
      and the matrix Hessian (:py:meth:`~sigcorr.models.model.AbstractModel.b_minus_loglike_hess`).

We start with the log-likelihood. It is a function of a vector of parameters ``p``, which
will be optimized to minimize the log-likelihood, and a ``sample`` of data (a toy):

.. code::

    import jax.numpy as jnp
    import jax.scipy as jsp
    from sigcorr.tools.utils import jax_jit_method
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = [..., "BG_NOISE_STD"]

        def __init__(self, xs):
            self.BG_NOISE_STD = 0.3
            self.xs = xs

        @jax_jit_method
        def b_minus_loglike(self, p, sample):
            return -jnp.sum(jsp.stats.norm.logpdf(sample,
                                                  loc=self.expected_b(*p),
                                                  scale=self.BG_NOISE_STD))

        @jax_jit_method
        def expected_b(self, n_bg, xscale):
            ...

Derivatives of the log-likelihood, similarly to the log-likelihood itself, are functions of
``p`` and ``sample``. These methods will be provided to :py:func:`scipy.optimize.minimize` to improve
the optimization process and should follow the :py:mod:`scipy` guidelines.

.. code::

    import jax.numpy as jnp
    import jax.scipy as jsp
    from sigcorr.tools.utils import jax_jit_method
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = [..., "BG_NOISE_STD"]

        def __init__(self, xs):
            ...
            self.BG_NOISE_STD = 0.3
            self.xs = xs

        @jax_jit_method
        def b_minus_loglike_grad(self, p, sample):
            B_i = self.expected_b(1, p[1])
            mu_i = self.expected_b(*p)
            deviation = mu_i - sample
            dp0 = jnp.sum(deviation*B_i)
            dp1 = -jnp.sum(deviation*mu_i*(self.xs - 100))
            return jnp.hstack([dp0, dp1])/self.BG_NOISE_STD**2

        @jax_jit_method
        def b_minus_loglike_hess(self, p, sample):
            B_shape = self.expected_b(1, p[1])
            mu = p[0]*B_shape
            deviation = mu - sample
            B_exp = self.xs - 100
            dp0p0 = jnp.sum(B_shape**2)
            dp0p1 = jnp.sum(-B_exp*(deviation + mu)*B_shape)
            dp1p1 = jnp.sum((deviation + mu)*B_exp**2*mu)
            return jnp.array([[dp0p0, dp0p1],
                              [dp0p1, dp1p1]])/self.BG_NOISE_STD**2

        @jax_jit_method
        def expected_b(self, n_bg, xscale):
            ...

Finally, we need to define the initial guess ``self.b_guess`` that
is convenient to derive from the model parameters with a slight perturbation. For this reason,
we prefer setting the initial guesses for the parameters (``self.b_guess``) in
the :py:meth:`~sigcorr.models.model.AbstractModel.init` method. This makes it easier to automate the derivation
of the initial guesses from the predefined model parameters and prevents the user from forgetting to update the initial guesses
when the model parameters are changed manually.

.. code::

    import numpy as np
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = ["B0", "BG_XSCALE", ...]

        def __init__(self, xs):
            self.B0 = 10
            self.BG_XSCALE = 0.033
            ...
            self.xs = xs

        def init(self):
            # we set the b_guess away from the local minimum (true values)
            # to prevent the optimizer getting stuck at the local minimum
            self.b_guess = np.array([self.B0, self.BG_XSCALE])*1.2
            return self


Signal + Background fits
------------------------


Similar components are required for the signal+background fits (sb-fits) to the background-only toys:

    * initial values for the parameters ``self.sb_guess``
    * "log-likelihood" function :py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike`
    * derivatives of the log-likelihood: :py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike_grad`
      and :py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike_hess`.

The arguments of these functions, however, are a little bit more complex. For example,
:py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike`:


.. code::

    import jax.numpy as jnp
    import jax.scipy as jsp
    from sigcorr.tools.utils import jax_jit_method
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = [..., "BG_NOISE_STD", "SIG_STD"]

        def __init__(self, xs):
            ...
            self.SIG_STD = 5.
            self.BG_NOISE_STD = 0.3
            self.xs = xs

        @jax_jit_method
        def sb_minus_loglike(self, p, sample, signal_x):
            return -jnp.sum(jsp.stats.norm.logpdf(sample,
                                                  loc=self.expected_sb(signal_x, p[:1], p[1:]),
                                                  scale=self.BG_NOISE_STD))

        @jax_jit_method
        def expected_sb(self, sig_x, sig_params, bg_params):
            return self.expected_signal(sig_x, *sig_params) + self.expected_b(*bg_params)

        @jax_jit_method
        def expected_signal(self, signal_x, n_sig):
            return n_sig*jsp.stats.norm.pdf(self.xs, loc=signal_x, scale=self.SIG_STD)

        @jax_jit_method
        def expected_b(self, n_bg, xscale):
            ...

The main differences to b-fits:

    * ``p`` is now a vector of both signal and background parameters. The
      :py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike` and derivative methods are responsible
      for the correct usage of the signal and background parameters.
    * ``signal_x`` that is one value in the 1D case (e.g., the position of an invariant mass peak), but can be a list of parameters in more dimensions (e.g., the position and width of an invariant mass peak). These are the
      signal parameters that are not present under the background hypothesis.

.. NOTE::
   Methods ``expected_b``, ``expected_signal``, ``expected_sb`` are optional. They are, however, convenient
   for the definitions of not only log-likelihoods but also gradients and hessians.

Similarly we define the derivatives. The sb-fit has 3 free parameters, therefore,
:py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike_grad` has output of length 3, and
:py:meth:`~sigcorr.models.model.AbstractModel.sb_minus_loglike_hess` returns a 3x3 matrix:

.. code::

    import jax.numpy as jnp
    import jax.scipy as jsp
    from sigcorr.tools.utils import jax_jit_method
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = [..., "BG_NOISE_STD"]

        def __init__(self, xs):
            ...
            self.BG_NOISE_STD = 0.3
            self.xs = xs

    @jax_jit_method
    def sb_minus_loglike_grad(self, p, sample, signal_x):
        S_i = self.expected_signal(signal_x, 1)
        B_i = self.expected_b(1, p[2])
        mu_i = self.expected_sb(signal_x, p[:1], p[1:])
        deviation = mu_i - sample
        dp0 = jnp.sum(deviation*S_i)
        dp1 = jnp.sum(deviation*B_i)
        dp2 = -jnp.sum(deviation*p[1]*B_i*(self.xs - 100))
        return jnp.hstack([dp0, dp1, dp2])/self.BG_NOISE_STD**2

    @jax_jit_method
    def sb_minus_loglike_hess(self, p, sample, signal_x):
        S_shape = self.expected_signal(signal_x, 1)
        B_shape = self.expected_b(1, p[2])
        B = p[1]*self.expected_b(1, p[2])
        mu = self.expected_sb(signal_x, p[:1], p[1:])
        deviation = mu - sample
        B_exp = (self.xs - 100)
        dp0p0 = jnp.sum(S_shape**2)
        dp0p1 = jnp.sum(S_shape*B_shape)
        dp0p2 = -jnp.sum(S_shape*B_exp*B)
        dp1p1 = jnp.sum(B_shape**2)
        dp1p2 = jnp.sum(-B_exp*(deviation + B)*B_shape)
        dp2p2 = jnp.sum((deviation + B)*B_exp**2*B)
        return jnp.array([[dp0p0, dp0p1, dp0p2],
                          [dp0p1, dp1p1, dp1p2],
                          [dp0p2, dp1p2, dp2p2]])/self.BG_NOISE_STD**2

        @jax_jit_method
        def expected_sb(self, sig_x, sig_params, bg_params):
            ...

        @jax_jit_method
        def expected_signal(self, signal_x, n_sig):
            ...

        @jax_jit_method
        def expected_b(self, n_bg, xscale):
            ...


Finally, we define the required initial values of the fit ``self.sb_guess``:

.. code::

    import numpy as np
    from sigcorr.models.model import AbstractModel

    class HyyTutorial(AbstractModel):
        PARAMS = ["B0", "BG_XSCALE", ...]

        def __init__(self, xs):
            self.B0 = 10
            self.BG_XSCALE = 0.033
            ...
            self.xs = xs

        def init(self):
            # we set the sb_guess away from the local minimum (true values)
            # to prevent the optimizer from getting stuck
            self.sb_guess = np.array([0.1, self.B0, self.BG_XSCALE])*1.2
            return self

At this point the |project| model ``HyyTutorial`` is fully defined.


Additional model methods
------------------------

There are some tools in |project| which may be also used with a |project| model,
but the model should implement some additional methods.

Estimation of the covariance matrix by constructing a linear expansion of the model is such an additional method,
and it requires the model to implement the :py:class:`~sigcorr.models.model.LinearApproximable` protocol, i.e. two methods:

* :py:meth:`~sigcorr.models.model.LinearApproximable.expected_b` - background only shape,
  which we are already familiar with and have implemented for ``HyyTutorial``,
* :py:meth:`~sigcorr.models.model.LinearApproximable.expected_b_grad` - a matrix of derivatives of this shape
  with respect to background model parameters.

Possible expansions of |project| may include more such protocols and extra methods.
