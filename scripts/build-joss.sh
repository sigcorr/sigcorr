#!/bin/bash
set -x

ROOT_DIR=$(realpath $(dirname "$0")/../)

docker run --rm \
    --volume $ROOT_DIR/docs/misc/2022-joss-paper:/data \
    --user $(id -u):$(id -g) \
    --env JOURNAL=joss \
    openjournals/inara
