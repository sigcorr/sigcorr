import h5py
import matplotlib.pyplot as plt
from sigcorr.tools.stats.utils import get_delta_ts


bf_res = None
with h5py.File("../data/3K1D/hyy_tutorial.h5", "r") as fin:
    fields = ["b_loglikes", "sb_loglikes", "sb_params"]
    bf_res = {k: fin[k][:3000, ...] for k in fields}
    xs = fin["scan_xs"][...].ravel()


ts = get_delta_ts(bf_res["b_loglikes"], bf_res["sb_loglikes"])
plt.plot(xs, ts[0, ...], label="test statistic")
plt.xlabel("m, GeV")
plt.ylabel("q")
plt.title("Test statistic curve")
plt.legend()


if __name__ == "__main__":
    plt.show()
