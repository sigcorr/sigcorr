Utils
=====

.. automodule:: sigcorr.tools.utils
   :undoc-members:
   :members: jax_jit_method, parse_grid_file, get_last_from_iter
