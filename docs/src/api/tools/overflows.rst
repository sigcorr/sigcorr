Overflows
=========

.. automodule:: sigcorr.tools.overflows
   :undoc-members:
   :members: overflows_along_zero_ax
