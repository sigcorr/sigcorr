import sys
from typing import Optional

import numpy as np
import scipy as sp
from scipy import stats


def get_delta_sigs(b_loglikes: np.ndarray, sb_loglikes: np.ndarray, signs: Optional[np.ndarray] = None) -> np.ndarray:
    """Compute significance from the the maximized background and signal+background log-likelihoods.

    Assumes that the test statistic follows the chi-squared distribution with 1 d.o.f:

    .. math::

        &TS = -2 (\log \mathcal{L}_{b} - \log \mathcal{L}_{sb})

        &Z = \pm \sqrt{TS}

    In the case of 1 d.o.f. it is possible to infer the sign of the underlying standard normal random variable [1]_.

    When the ``signs`` are provided, and they are shaped as ``sb_loglikes``,
    then the resulting significance values use the signs of the corresponding values in ``signs``.

    Parameters
    ----------
        b_loglikes : np.ndarray
            Array of maximized background log-likelihoods. Shaped as ``(n_samples, )``
        sb_loglikes : np.ndarray
            Array of maximized signal+background log-likelihoods. Shaped as ``(n_samples, ...)``
        signs : Optional[np.ndarray]
            Array of values signs of which correspond to the signs of the significances.
            Shaped as ``sb_loglikes.shape``

    Returns
    -------
        sigs : np.ndarray
            Array of significances shaped as ``sb_loglikes.shape``.

    References
    ----------

    .. [1] G. Cowan and K. Cranmer and E. Gross et al. "Asymptotic formulae for likelihood-based tests of new physics,"
            Eur. Phys. J. C 71, 1554 (2011), https://doi.org/10.1140/epjc/s10052-011-1554-0

    Examples
    --------

    In this example we have 3 background samples, and for each sample a likelihood scan on 4 points was made.
    We then generate some random ``signs`` to present the functionality and use them with loglikelihoods to estimate
    the significances.

    .. testcode::

        import numpy as np
        from numpy import random
        from sigcorr.tools.stats.utils import get_delta_sigs

        b_loglikes = np.array([1, 2, 3])
        sb_loglikes = np.array([[1.2, 1., 1.5, 1.4],
                                [2.3, 2.5, 2.1, 3.6],
                                [3.1, 3.1, 3.5, 3.]])
        signs = np.random.random((3, 4)) - 0.5  # random values between -0.5 to 0.5

        sigs = get_delta_sigs(b_loglikes, sb_loglikes, signs=signs)
    """
    ts_batch = get_delta_ts(b_loglikes, sb_loglikes)
    sigs_batch = np.sqrt(ts_batch)
    signed_sigs_batch = sigs_batch*np.sign(signs) if signs is not None else sigs_batch
    return signed_sigs_batch


def get_delta_ts(b_loglikes: np.ndarray, sb_loglikes: np.ndarray) -> np.ndarray:
    """Compute a test statistic from the the maximized background and signal+background log-likelihoods.

    .. math::

        TS = -2 (\log \mathcal{L}_{b} - \log \mathcal{L}_{sb})

    Parameters
    ----------
        b_loglikes : np.ndarray
            Array of maximized background log-likelihoods. Shaped as ``(n_samples, )``
        sb_loglikes : np.ndarray
            Array of maximized signal+background log-likelihoods. Shaped as ``(n_samples, ...)``

    Returns
    -------
        ts : np.ndarray
            Array of test statistics shaped as ``sb_loglikes.shape``.

    Examples
    --------

    In this example we compute test statistic values for
    3 toy background samples, where for each sample a likelihood scan on 4 points was made.

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.utils import get_delta_ts

        b_loglikes = np.array([1, 2, 3])
        sb_loglikes = np.array([[1.2, 1., 1.5, 1.4],
                                [2.3, 2.5, 2.1, 3.6],
                                [3.1, 3.1, 3.5, 3.]])

        ts = get_delta_ts(b_loglikes, sb_loglikes)
    """
    ts_dim = sb_loglikes.ndim - b_loglikes.ndim
    extra_b_axes = list(range(b_loglikes.ndim, b_loglikes.ndim + ts_dim))
    ts_batch = -2*(np.expand_dims(b_loglikes, axis=extra_b_axes) - sb_loglikes)
    ts_batch[ts_batch < 0] = 0
    return ts_batch


def pval2sig(pval: np.ndarray) -> np.ndarray:
    """Translate p-values to significances.

    .. math::

        Z = \Psi^{-1}(1 - p_{value})

    Here :math:`\Psi` is the CDF of the standard normal distribution.

    Parameters
    ----------
        pval : np.ndarray
            Array of on-sided p-values.

    Returns
    -------
        sigs : np.ndarray
            Array of significances.

    Examples
    --------

    .. testcode::

        from sigcorr.tools.stats.utils import pval2sig

        # one value
        sig = pval2sig(1E-3)

        # array
        sig = pval2sig(np.array([1E-2, 1E-3, 1E-5]))
    """
    return sp.stats.norm.isf(pval)


def sig2pval(sig: np.ndarray) -> np.ndarray:
    """Translate significances to p-values.

    .. math::

        p_{value} = 1 - \Psi(Z)

    Here :math:`\Psi` is the CDF of the standard normal distribution.

    Parameters
    ----------
        sig : np.ndarray
            Array of significances.

    Returns
    -------
        pval : np.ndarray
            Array of one-sided p-values.

    Examples
    --------

    .. testcode::

        from sigcorr.tools.stats.utils import sig2pval

        # one value
        pval = sig2pval(3.)

        # array
        pval = sig2pval(np.array([1, 2, 3.]))
    """
    return 1 - sp.stats.norm.cdf(sig)
