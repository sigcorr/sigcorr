import numpy as np
from numpy import polynomial
from numpy import linalg
import scipy as sp
from scipy import stats
from sigcorr.tools.stats.utils import sig2pval


class GPEulerNumberPropagator:
    r"""Propagate average Euler characteristic (number) of the Gaussian process between thresholds in nD case.

    For a Gaussian process of any dimension it is possible to estimate the average Euler characteristic
    of a set of points above a threshold (overflow set) if several reference values of
    the average Euler characteristics at some other levels are known.

    For n-dimensional Gaussian process one needs n reference levels and then it is possible
    to calculate the average Euler characteristic at any other level.

    The core of the method is the fact that the average Euler characteristic of the overflow set is
    a linear combination of the n Hermite polynomials in the n-dimensional case [1]_:

    .. math::

        <\text{Euler number(c)}> = P(Z > c | Z \sim \mathcal{N}(0, 1))
            +  \mathrm{e}^{-c^2/2} \sum_{i=0}^{n-1} C_i H_i(c),

    where :math:`H_i` are Hermite polynomials of the "probabilists" kind: :py:mod:`numpy.polynomial.hermite_e` and
    :math:`c` is the threshold of interest.

    This expression has also unknown :math:`C_i` that we can learn by solving the system of linear equations for
    :math:`n` different reference values of the average Euler characteristic in the n-dimensional case.

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.gp.euler_number import GPEulerNumberPropagator

        # 1D gaussian process
        ref_thresholds = np.array([1])
        ref_euler_nums = np.array([3.2])
        target_thresholds = np.array([0.5, 1.5, 2.5])
        target_euler_numbers = GPEulerNumberPropagator(ref_thresholds, ref_euler_nums).calc(target_thresholds)
        # array([4.73366803, 1.69472173, 0.22652394])

        # 2D gaussian process
        ref_thresholds = np.array([1, 2.])
        ref_euler_nums = np.array([3.2, 1.2])
        target_thresholds = np.array([0.5, 1.5, 2.5])
        target_euler_numbers = GPEulerNumberPropagator(ref_thresholds, ref_euler_nums).calc(target_thresholds)
        # array([3.10791656, 2.29280228, 0.46934814])

    References
    ----------

    .. [1] O. Vitells and E. Gross, "Estimating the significance of a signal in a multi-dimensional search,"
           Astroparticle Physics, Volume 35, Issue 5, 2011, Pages 230-234, ISSN 0927-6505,
           https://doi.org/10.1016/j.astropartphys.2011.08.005

    """
    def __init__(self, ref_sigs: np.array, ref_euler_nums: np.array):
        """
        Parameters
        ----------
            ref_sigs : np.array
                Reference levels at which we know the average Euler characteristics.
                Should be exactly n of them for the n-dimensional GP.
            ref_euler_nums : np.array
                Average Euler characteristics of the GP at thresholds ``ref_sigs``.

        """
        self.Ns = get_gp_euler_number_coefs(ref_sigs, ref_euler_nums)

    def calc(self, sigs: np.array) -> np.array:
        """Estimate average Euler characteristic for the overflow set of the Gaussian process at a threshold.

        Parameters
        ----------
            sigs : np.array
                Array of target thresholds of interest.

        Returns
        -------
            avg_euler_numbers : np.array
                Array of average Euler characteristics for the target thresholds of interest.
        """
        return 1 - sp.stats.norm().cdf(sigs) + np.polynomial.hermite_e.hermeval(sigs, self.Ns)*np.exp(-sigs**2/2)


def get_gp_euler_number_coefs(ref_sigs, ref_euler_nums):
    A = np.polynomial.hermite_e.hermevander(ref_sigs, ref_sigs.shape[0]-1)
    b = (ref_euler_nums - 1 + sp.stats.norm().cdf(ref_sigs))*np.exp(ref_sigs**2/2)
    x = np.linalg.solve(A, b)
    return x


def avg_gp_euler_number(local_sig: np.array, avg_upcross: np.array) -> np.array:
    r"""Average Euler characteristic (number) of the overflow set of the Gaussian process from the average number
    of up-crossings.

    An important characteristic of a curve is the Euler characteristic of the set of points of this curve above some
    threshold (overflow set), and it is a function of the threshold.

    The average Euler characteristic for a curve above threshold consists of two components:

        * average number of up-crossings of the threshold,
        * a boundary term that accounts for the up-crossing that we didn't notice because it happend behind
          the left edge of the scanning region while still contributing to the peak that is observed in the region.

    The boundary term is just a probability to observe the edge point above the threshold, or alternatively
    it is the p-value of the test statistic that can be computed from the cdf of the chi-squared distribution.

    For the Gaussian process curves we can write more formally, assuming that the GP has 0-mean and the covariance
    matrix has 1 on the diagonal (the case of the likelihood scan and the resulting significance curve):

    .. math::

        <\text{Euler number}(c)> = P(Z > c | Z \sim \mathcal{N}(0, 1)) + <\text{up-crossings}(c)>


    Parameters
    ----------
        local_sig : np.array
            The level of interest. We know the average number of up-crossing for it.
        avg_upcross : np.array
            The average number of up-crossings at level ``local_sig``.

    Returns
    -------
        average_euler_number : np.array
            Average Euler number of the overflow set for the Gaussian process.

    Examples
    --------
    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.gp.euler_number import avg_gp_euler_number

        thresholds = np.array([1.2, 2.1, 3.5])
        avg_upcrossings = np.array([5, 4., 3.1])
        euler_number = avg_gp_euler_number(thresholds, avg_upcrossings)
        # array([5.11506967, 4.01786442, 3.10023263])
    """
    return sig2pval(local_sig) + avg_upcross
