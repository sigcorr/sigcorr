Up-crossings
============

.. automodule:: sigcorr.tools.stats.ts.upcross
   :undoc-members:
   :members: propagate_upcross
