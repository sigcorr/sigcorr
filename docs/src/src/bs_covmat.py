import h5py
import matplotlib.pyplot as plt
from sigcorr.mapreduce.file import h5_batch_mapreduce
from sigcorr.mapreduce.map_reducers import SigsCalc
from sigcorr.mapreduce.map_reducers import BatchStats2Reduce
from sigcorr.tools.utils import get_last_from_iter


with h5py.File("../data/3K1D/hyy_tutorial.h5", "r") as fin:
    xs = fin["scan_xs"][...].ravel()


bs, _ = get_last_from_iter(h5_batch_mapreduce("../data/3K1D/hyy_tutorial.h5",
                                              ["b_loglikes", "sb_loglikes", "sb_params"],
                                              200,
                                              SigsCalc(),
                                              BatchStats2Reduce()))
cov = bs.get_cov()
cov_err = bs.get_cov_stat_err()


fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(12, 4))

mesh = ax[0].pcolormesh(xs, xs, cov)
ax[0].set_xlabel("m, GeV")
ax[0].set_ylabel("m, GeV")
ax[0].invert_yaxis()
ax[0].set_title("Covariance matrix")
plt.colorbar(mesh, ax=ax[0])

mesh = ax[1].pcolormesh(xs, xs, cov_err)
ax[1].set_title("Statistical error")
ax[1].set_xlabel("m, GeV")
ax[1].set_ylabel("m, GeV")
ax[1].invert_yaxis()
plt.colorbar(mesh, ax=ax[1])


if __name__ == "__main__":
    plt.show()
