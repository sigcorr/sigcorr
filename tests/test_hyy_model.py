import unittest

import jax
import numpy as np
from numpy import random

from sigcorr.models.hyy import Hyy


REL_DELTA = 1E-7


class TestHyy(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.xs = np.arange(100, 160, 0.5)

    def setUp(self):
        np.random.seed(148)

    def test_hyy_b_grad(self):
        for _ in range(10):
            self._one_hyy_b_grad()

    def _one_hyy_b_grad(self):
        model = Hyy(self.xs).init()
        p = np.array([model.B0, model.BG_XSCALE], dtype=np.float32)

        sample = model.get_bg_samples(1)[0]

        test_b_loglike_grad = model.b_minus_loglike_grad(p, sample)
        true_b_loglike_grad = jax.grad(model.b_minus_loglike)(p, sample)
        self.assertAlmostEqual(np.max(np.absolute((test_b_loglike_grad - true_b_loglike_grad)/true_b_loglike_grad)),
                               0, delta=REL_DELTA)

    def test_hyy_sb_grad(self):
        for _ in range(10):
            self._one_hyy_sb_grad()

    def _one_hyy_sb_grad(self):
        model = Hyy(self.xs).init()
        p = np.array([0.1, model.B0, model.BG_XSCALE], dtype=np.float32)
        signal_x = self.xs[25]

        sample = model.get_bg_samples(1)[0]

        test_sb_loglike_grad = model.sb_minus_loglike_grad(p, sample, signal_x)
        true_sb_loglike_grad = jax.grad(model.sb_minus_loglike)(p, sample, signal_x)
        self.assertAlmostEqual(np.max(np.absolute((test_sb_loglike_grad - true_sb_loglike_grad)/true_sb_loglike_grad)),
                               0, delta=REL_DELTA)

    def test_hyy_b_hess(self):
        for _ in range(10):
            self._one_hyy_b_hess()

    def _one_hyy_b_hess(self):
        model = Hyy(self.xs).init()
        p = np.array([model.B0, model.BG_XSCALE], dtype=np.float64)

        sample = model.get_bg_samples(1)[0]

        test_b_loglike_hess = model.b_minus_loglike_hess(p, sample)
        true_b_loglike_hess = jax.hessian(model.b_minus_loglike)(p, sample)
        self.assertAlmostEqual(np.max(np.absolute((test_b_loglike_hess - true_b_loglike_hess)/true_b_loglike_hess)),
                               0, delta=REL_DELTA)

    def test_hyy_sb_hess(self):
        for _ in range(10):
            self._one_hyy_sb_hess()

    def _one_hyy_sb_hess(self):
        model = Hyy(self.xs).init()
        p = np.array([0.1, model.B0, model.BG_XSCALE], dtype=np.float64)
        signal_x = self.xs[25]

        sample = model.get_bg_samples(1)[0]

        test_sb_loglike_hess = model.sb_minus_loglike_hess(p, sample, signal_x)
        true_sb_loglike_hess = jax.hessian(model.sb_minus_loglike)(p, sample, signal_x)
        self.assertAlmostEqual(np.max(np.absolute((test_sb_loglike_hess - true_sb_loglike_hess)/true_sb_loglike_hess)),
                               0, delta=REL_DELTA)
