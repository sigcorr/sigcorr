import numpy as np
import scipy as sp
from scipy import stats
from scipy import integrate

from sigcorr.tools.derivative import derivative_along_axes


def gp_upcross_at_level(xs: np.ndarray, K: np.ndarray, u: np.number) -> np.number:
    """Estimate average number of up-crossings for a Gaussian process.

    From the properties of the Gaussian process it is possible to derive
    the analytical expression for the up-crossings density that depends
    on the covariance kernel and its derivatives.

    For numerical purposes, the kernel is defined on the grid ``xs``, and
    it becomes a covariance matrix.

    We use 2-dimensional cubic splines to smooth the covariance matrix both
    to evaluate the covariance matrix on a finer grid for integration
    and to estimate its derivatives at any point.

    To transition from the up-crossings density to the average number of up-crossings,
    in this implementation, we use Simpson quadrature and intergrate the density
    over the range defined by ``xs``.

    The expression itself is quite complex so we don't show it here, however,
    there is a high level description of it in [1]_ and more thorough derivation
    in [2]_.

    We also understand and warn the user that smoothing, differentiation and
    integration are complex steps and the current implementation of this function
    has parameter settings that might not be optimal in other situations.

    Users, however, can assemble their own method while re-using some building
    blocks from the source code: :src:`sigcorr/tools/stats/gp/upcross.py`.

    Parameters
    ----------
        xs : np.ndarray
            Coordinates for axes of the covariance matrix.
        K : np.ndarray
            Covariance matrix of the Gaussian process defined on the grid ``xs``.
        u : np.number
            Level at which we want to estimate the up-crossings.

    Returns
    -------
        average_gp_upcrossings : np.number
            Average number of up-crossings for the Gaussian process.

    References
    ----------

    .. [1] V. Ananyev and A. L. Read, "Gaussian Process-based calculation of look-elsewhere trials factor,"
           JINST 18 (2023) P05041, https://doi.org/10.1088/1748-0221/18/05/P05041

    .. [2] L. D. Lutes and S. Sarkani, "Random vibrations,"
           Butterworth-Heinemann, 978-0-7506-7765-3, https://doi.org/10.1016/B978-0-7506-7765-3.X5000-2

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.gp.upcross import gp_upcross_at_level

        xs = np.arange(0, 5, 0.2)
        K = np.eye(xs.shape[0]) + 0.1
        level = 0.5
        upcross_at_level = gp_upcross_at_level(xs, K, level)
        # 4.18 - estimated average number of up-crossings

    """
    density = gp_upcross_density(xs, K, u)
    return integrate_gp_upcross_density(density, xs)


def integrate_gp_upcross_density(density, xs):
    return sp.integrate.simpson(density, xs)


def gp_upcross_density(xs, K, u):
    return _gp_upcross_density(xs, K, u, derivative_along_axes)


def _gp_upcross_density(xs, K, u, derivative_func):
    K_xdy = derivative_func(K, [xs, xs], [0, 1])
    K_dxdy = derivative_func(K, [xs, xs], [1, 1])

    sigma_x = np.sqrt(np.diagonal(K))
    sigma_dx = np.sqrt(np.diagonal(K_dxdy))

    rho_xdy = np.diagonal(K_xdy)/sigma_x/sigma_dx

    mu_star = rho_xdy*sigma_dx/sigma_x
    sigma_star = sigma_dx*np.sqrt(1 - rho_xdy**2)

    return 1/np.sqrt(2*np.pi)/sigma_x*np.exp(-u**2/2/sigma_x**2) * \
        (mu_star*u*sp.stats.norm.cdf(mu_star/sigma_star*u) +
         sigma_star/np.sqrt(2*np.pi)*np.exp(-mu_star**2/2/sigma_star**2*u**2))


def gp_upcross_density_stationary(xs, K, u):
    return _gp_upcross_density_stationary(xs, K, u, derivative_along_axes)


def _gp_upcross_density_stationary(xs, K, u, derivative_func):
    K_dxdy = derivative_func(K, [xs, xs], [1, 1])
    sigma_x = np.sqrt(np.diagonal(K))
    sigma_dx = np.sqrt(np.diagonal(K_dxdy))
    return 1/2/np.pi*sigma_dx/sigma_x*np.exp(-u**2/2/sigma_x**2)
