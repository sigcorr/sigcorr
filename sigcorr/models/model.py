from __future__ import annotations
from typing import (
    List,
    Protocol,
)
from typing_extensions import Self
import numpy as np


class AbstractModel:
    """Abstract class for |project| model

    Every model should implement the following set of operations:

        * model parameters + ``init()`` method for them
        * method for sampling background samples
        * background model, its derivatives, starting point for the optimization
        * signal + background model, its derivatives, starting point for the optimization

    Attributes
    ----------
    PARAMS : List[str]
        List of model param names. Parameters will be stored to file when the model
        is dumped and restored back based on this list.
    """

    PARAMS: List[str] = []

    def __init__(self, xs: np.ndarray):
        """
        Properties defined here are required, and are used by other utils in |project|. However,
        they can also be defined in :py:meth:`init`.

        Parameters
        ----------
        xs : np.ndarray
            A grid on which the background model is defined.

        Examples
        --------
        .. code-block::

            def __init__(self, xs):
                # Required
                self.xs = xs

                # Required. Initial guess for the background model parameters used in scipy optimize
                self.b_guess: np.ndarray

                # Required. Initial guess for the signal+background model parameters used in scipy optimize
                self.sb_guess: np.ndarray
        """
        self.xs = xs
        self.b_guess: np.ndarray = None
        self.sb_guess: np.ndarray = None

    def init(self) -> AbstractModel:
        """Post-initializer. Should always be called. Call it after values of the parameters were set.

        Defines some paramters that can be derived from the model parameters.

        Examples
        --------
        .. code-block::

            def init(self):
                self.b_guess = np.array([self.p1])
                self.sb_guess =  np.array([self.p1**2, self.p2/self.p1])
                return self

        """
        return self

    def get_bg_samples(self, num_samples: int) -> np.ndarray:
        """Produce a batch of background samples.

        Parameters
        ----------
        num_samples : int
            Number of samples to be produced.

        Returns
        -------
        np.ndarray
            First axis enumerates the samples, so has exactly ``num_samples`` elements,
            rest of the axes correspond to the shape of the background sample.

            Output shape: ``(num_samples, *self.xs.shape)``

        """
        raise NotImplementedError()

    def b_minus_loglike(self, p: np.ndarray, sample: np.ndarray):
        """Returns ``-log(likelihood ratio)`` for the background model.

        It is passed to ``sp.optimize`` to estimate model parameters with MLE.

        Parameters
        ----------
        p: np.ndarray
            Array of paramters that will be optimized for the background model.
            Has same shape as ``self.b_guess``.
        sample: np.ndarray
            One background sample for which the likelihood will be optimized
        """
        raise NotImplementedError()

    def b_minus_loglike_grad(self, p: np.ndarray, sample: np.ndarray):
        """Gradient for :py:meth:`b_minus_loglike`

           Passed to ``sp.optimize`` together with :py:meth:`b_minus_loglike`

           Parameters
           ----------
           * : * :py:meth:`b_minus_loglike`
        """
        raise NotImplementedError()

    def b_minus_loglike_hess(self, p: np.ndarray, sample: np.ndarray):
        """Hessian for :py:meth:`b_minus_loglike`

           Passed to ``sp.optimize`` together with :py:meth:`b_minus_loglike`

           Parameters
           ----------
           * : * :py:meth:`b_minus_loglike`
        """

    def sb_minus_loglike(self, p: np.ndarray, sample: np.ndarray, signal_x: np.ndarray) -> np.ndarray:
        """Returns ``-log(likelihood ratio)`` for the background + signal model

        It is passed to ``sp.optimize`` to estimate model parameters with MLE.

        Parameters
        ----------
        p: np.ndarray
            Array of paramters that will be optimized for the background model.
            Has same shape as ``self.sb_guess``.
        sample: np.ndarray
            One background sample for which the likelihood will be optimized
        signal_x: np.ndarray
            Value of the nuisance parameter not present under background hypothesis.
            If there are more than 1, ``signal_x`` can be a vector.
        """
        raise NotImplementedError()

    def sb_minus_loglike_grad(self, p: np.ndarray, sample: np.ndarray, signal_x: np.ndarray) -> np.ndarray:
        """Gradient for :py:meth:`sb_minus_loglike`

           Passed to ``sp.optimize`` together with :py:meth:`sb_minus_loglike`

           Parameters
           ----------
           * : * :py:meth:`sb_minus_loglike`
        """
        raise NotImplementedError()

    def sb_minus_loglike_hess(self, p: np.ndarray, sample: np.ndarray, signal_x: np.ndarray) -> np.ndarray:
        """Hessian for :py:meth:`sb_minus_loglike`

           Passed to ``sp.optimize`` together with :py:meth:`sb_minus_loglike`

           Parameters
           ----------
           * : * :py:meth:`sb_minus_loglike`
        """
        raise NotImplementedError()

    def copy(self) -> Self:
        """Create a copy instance of the model
        """
        new_xs = self.xs.copy()
        new_model = self.__class__(new_xs)
        for p in self.PARAMS:
            setattr(new_model, p, getattr(self, p))
        return new_model


class LinearApproximable(Protocol):
    """ Interface for linear approximation estimates of the significance covariance matrix

        The model should expose:

        * shape of the injected signal
        * derivative of the background-only template with respect to each model parameter

        Examples
        --------
        .. testcode::

            import numpy as np
            import scipy as sp
            from sigcorr.models.model import (
                AbstractModel,
                LinearApproximable,
            )


            class ExampleModel(AbstractModel, LinearApproximable):
                def __init__(self, xs):
                    self.xs = xs
                    ...

                ...

                def expected_signal(self, signal_x, n_sig):
                    return n_sig*sp.stats.norm.pdf(self.xs, loc=signal_x, scale=1.23)

                def expected_b(self, a, b):
                    return a*self.xs + a*b**2*self.xs**2

                # assuming a and b are arrays of shape (num_samples,)
                def expected_b_grad(self, a, b):
                    a_ext = np.expand_dims(a, (-1, -2))  # shape (num_samples, 0, 0)
                    b_ext = np.expand_dims(b, (-1, -2))  # shape (num_samples, 0, 0)
                    xs_ext = np.expand_dims(self.xs, (0, 1))  # shape (0, 0, xs)

                    # after broadcasting results into shape (num_samples, bg_params, xs)
                    return np.concatenate([
                        xs_ext + b_ext**2*xs_ext**2,
                        2*a_ext*b_ext*xs_ext**2,
                    ], axis=1)


            xs = np.array([1, 2, 3, 4])  # xs = 4
            a = b = np.array([1, 2, 3])  # num_samples = 3, bg_params = 2

            # print(ExampleModel(xs).expected_b_grad(a, b).shape)
            # (3, 2, 4)

        .. seealso::
            :py:func:`sigcorr.linear_approx_sigcov.get_linear_approx_sigcov_for_model`,
            :py:func:`sigcorr.tools.stats.ts.sigcov_linear_model.get_sigcov_for_linear_model`

    """
    def expected_signal(self, signal_x, *s_params) -> np.ndarray:
        """ Signal shape

        Parameters
        ----------
        signal_x : np.ndarray
            Array of signal locations. Only 1-D models are supported, therefore ``signal_x.shape = (xs,)``.

        *s_params : np.ndarray
            A tuple of signal parameters.
            For the linear approximation a tuple of constant values should be supported.
        """
        raise NotImplementedError()

    def expected_b_grad(self, *b_params) -> np.ndarray:
        """ Derivative of the background only template with respect to each model parameter

        Parameters
        ----------
        *b_params : np.ndarray
            A tuple of arrays of shape ``(num_samples,)``,
            where each array represents ``num_samples`` values of the corresponding background parameter.

        Returns
        -------
        gradient: np.ndarray
            An array of shape ``(num_samples, num_b_params, xs)`` where each cell represents
            a derivative of the background shape with for a specific sample, parameter and point on x-axis.
        """
        raise NotImplementedError()
