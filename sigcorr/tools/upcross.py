"""
Up-crossing is a local feature of a curve and is the point on the x-axis where the curve
crosses some level from below to above. The functions in this module work with
up-crossings of level 0, therefore, the crossings of the x-axis by the curves.
"""

import numpy as np


def zero_upcross_count(sigs: np.ndarray) -> np.ndarray:
    """Count number of zero-up-crossings for every curve in the input.

    Parameters
    ----------
        sigs : np.ndarray
            Input array of curves. The last axis enumerates the points of 1 curve, and the rest of the dimensions
            enumerate the curves themselves.

    Returns
    -------
    np.ndarray
        An array with the shaped of the first N-1 dimensions of the input (1 cell per curve),
        where each cell contains the number of up-crossings of the level zero for the corresponding curve.

    Examples
    --------
    .. testcode::

        import numpy as np
        from sigcorr.tools.upcross import zero_upcross_count

        xs = np.linspace(0, np.pi, 100)
        curves = np.vstack([np.cos(xs), np.cos(xs*2), np.cos(xs*3), np.cos(xs*4)])
        upcross_count = zero_upcross_count(curves)
        # [0 (down), 1 (down+up), 1 (down+up+down), 2 (down+up+down+up)]
    """
    return np.count_nonzero(zero_upcross_locs(sigs), axis=-1)


def zero_upcross_locs(sigs: np.ndarray) -> np.ndarray:
    """Detect locations of the up-crossings for each curve in the input.

    Parameters
    ----------
        sigs : np.ndarray
            Input array of curves. The last axis enumerates the points of 1 curve, and the rest of the dimensions
            enumerate curves themselves.

    Returns
    -------
    np.ndarray
        An array with the same shape as the input. The last dimension corresponds to one curve,
        where cell contains ``True`` if it is the location of up-crossing or ``False`` otherwise.

    Examples
    --------
    .. testcode::

        import numpy as np
        from sigcorr.tools.upcross import zero_upcross_locs

        xs = np.linspace(0, np.pi, 100)
        curves = np.vstack([np.cos(xs), np.cos(xs*2), np.cos(xs*3), np.cos(xs*4)])
        upcross_count = zero_upcross_locs(curves)
        # [[False, False, ..., False, False, False, ...],
        #  [False, False, ..., True, False, False, ...],
        #  [False, False, ..., True, False, False, ...],
        #  [False, False, True, False, ..., False, False, True, False, ...]]
    """
    return (sigs[..., 1:] > 0) & (sigs[..., :-1] <= 0)
