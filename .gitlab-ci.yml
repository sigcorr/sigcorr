# dev flow: run pipeline on merge requests
# if "no-ci" label is present in the MR, dont run
# if "build-pages" label is present in the MR, build docs for MR
#
# release flow: run pipeline on tags of form 1.2.3 (should be manually triggered)
#
# both "dev" and "latest" docs are latest stable docs, because dev was mentioned in the publication as a reference.

stages:
  - test
  - prepare_docs
  - deploy_docs

.run_unittests:
  script:
    - python -m pip install .[test]
    # Increase precision
    - . env.sh
    - pytest tests/*

.build_docs:
  variables:
    GIT_FETCH_EXTRA_FLAGS: "toy_data --prune"
  script:
    # Plots for docs cover a large part of the API
    - python -m pip install .[doc]
    - ./scripts/build-docs.sh
    - python -m pip freeze > build/docs/requirements.txt

.ci_test:
  variables: !reference [.build_docs, variables]
  before_script:
    - python -m venv venv
    - . venv/bin/activate
    - python -m pip install --upgrade pip setuptools wheel
  script:
    - !reference [.run_unittests, script]
    - !reference [.build_docs, script]
      # doctest is cheap when docs were built
    - ./scripts/run-doctest.sh

test_python3.11:
  stage: test
  extends: .ci_test
  image: python:3.11
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_LABELS !~ "/no-ci/"
    - if: $CI_COMMIT_TAG =~ "/^[0-9]+.[0-9]+.[0-9]+$/"
      when: manual

test_python3.10:
  stage: test
  extends: .ci_test
  image: python:3.10
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_LABELS !~ "/no-ci/"
    - if: $CI_COMMIT_TAG =~ "/^[0-9]+.[0-9]+.[0-9]+$/"
      when: manual

test_python3.9:
  stage: test
  extends: .ci_test
  image: python:3.9
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_LABELS !~ "/build-pages/" && $CI_MERGE_REQUEST_LABELS !~ "/no-ci/"

test_and_store_docs_python3.9:
  stage: test
  extends: .ci_test
  image: python:3.9
  artifacts:
    paths:
      - build/docs/
  rules:
    - if: $CI_COMMIT_TAG =~ "/^[0-9]+.[0-9]+.[0-9]+$/"
      when: manual
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_LABELS =~ "/build-pages/" && $CI_MERGE_REQUEST_LABELS !~ "/no-ci/"

prepare_docs:
  stage: prepare_docs
  script:
    - 'rm -rf public/;'
    - 'wget https://sigcorr.gitlab.io/sigcorr/dump.tar.gz && tar -xvzf dump.tar.gz;'
    - 'rm -rf public/dev public/latest;'
    - 'mkdir -p public/'
    - 'cp -r build/docs public/dev;'
    - 'cp -r build/docs public/latest;'
    - 'rm -rf public/v/$CI_COMMIT_TAG;'
    - 'mkdir -p public/v/;'
    - 'cp -r build/docs public/v/$CI_COMMIT_TAG;'
    - 'rm -f dump.tar.gz;'
    - 'tar -cvzf dump.tar.gz public/;'
    - 'mv dump.tar.gz public/;'
  artifacts:
    paths:
      - public/
  rules:
    - if: $CI_COMMIT_TAG =~ "/^[0-9]+.[0-9]+.[0-9]+$/"

prepare_branch_docs:
  stage: prepare_docs
  script:
    - 'rm -rf public/;'
    - 'wget https://sigcorr.gitlab.io/sigcorr/dump.tar.gz && tar -xvzf dump.tar.gz;'
    - 'rm -rf public/branches/$CI_COMMIT_REF_SLUG;'
    - 'mkdir -p public/branches/;'
    - 'cp -r build/docs public/branches/$CI_COMMIT_REF_SLUG;'
    - 'rm -f dump.tar.gz;'
    - 'tar -cvzf dump.tar.gz public/;'
    - 'mv dump.tar.gz public/;'
  artifacts:
    paths:
      - public/
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_LABELS =~ "/build-pages/"&& $CI_MERGE_REQUEST_LABELS !~ "/no-ci/"

pages:
  stage: deploy_docs
  script:
    - echo "Deploying docs!"
  artifacts:
    paths:
      - public/
  rules:
    - if: $CI_COMMIT_TAG =~ "/^[0-9]+.[0-9]+.[0-9]+$/"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_LABELS =~ "/build-pages/"&& $CI_MERGE_REQUEST_LABELS !~ "/no-ci/"
