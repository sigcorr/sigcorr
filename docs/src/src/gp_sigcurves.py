import h5py
import numpy as np
import matplotlib.pyplot as plt
from sigcorr.tools.stats.utils import get_delta_sigs
from sigcorr.tools.stats.batch_stats import BatchStats2
from sigcorr.tools.stats.gp.sampling import draw_gp_svd


bf_res = None
with h5py.File("../data/3K1D/hyy_tutorial.h5", "r") as fin:
    fields = ["b_loglikes", "sb_loglikes", "sb_params"]
    bf_res = {k: fin[k][:3000, ...] for k in fields}
    xs = fin["scan_xs"][...].ravel()


sigs = get_delta_sigs(bf_res["b_loglikes"], bf_res["sb_loglikes"], bf_res["sb_params"][..., 0])

num_samples = sigs.shape[0]
sample_dim = sigs.shape[1]
bs = BatchStats2(num_samples, sample_dim)
bs.push(sigs)
corr = bs.get_corr(override_x=np.array([0.]))
corr_err = bs.get_corr_stat_err(override_x=np.array([0.]))

gp_sigs = draw_gp_svd(corr, 5)

for i in range(gp_sigs.shape[0]):
    plt.plot(xs, gp_sigs[i, ...])
plt.xlabel("m, GeV")
plt.ylabel("Z")
plt.title("GP significance curves")


if __name__ == "__main__":
    plt.show()
