import h5py
import numpy as np
import matplotlib.pyplot as plt
from sigcorr.models.hyy import Hyy
from sigcorr.models.gross_vitells import GrossVitells
from sigcorr.linear_approx_sigcov import get_linear_approx_sigcov_for_model


bf_res = None
with h5py.File("../data/3K1D/hyy_tutorial.h5", "r") as fin:
    fields = ["b_params"]
    bf_res = {k: fin[k][:1, ...] for k in fields}
    xs1 = fin["scan_xs"][...].ravel()


s_params = (1, )  # signal has only normalization constant which does not affect the result

model = Hyy(xs1).init()
b_params = bf_res["b_params"][0, np.newaxis]
cov1 = get_linear_approx_sigcov_for_model(model, xs1, b_params, s_params)[0]

# based on the Gross and Vitells model introduced in 10.1140/epjc/s10052-010-1470-8
# with covariance matrix estimated in 10.1088/1748-0221/18/05/P05041
xs2 = np.arange(5, 120.1, 0.25)
model = GrossVitells(xs2).init()
b_params = np.array([[1.]])
cov2 = get_linear_approx_sigcov_for_model(model, xs2, b_params, s_params)[0]

fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(12, 4))

mesh = ax[0].pcolormesh(xs1, xs1, cov1)
ax[0].set_xlabel("m, GeV")
ax[0].set_ylabel("m, GeV")
ax[0].invert_yaxis()
ax[0].set_title("Hyy")
plt.colorbar(mesh, ax=ax[0])

mesh = ax[1].pcolormesh(xs2, xs2, cov2)
ax[1].set_title("GrossVitells")
ax[1].set_xlabel("m, GeV")
ax[1].set_ylabel("m, GeV")
ax[1].invert_yaxis()
plt.colorbar(mesh, ax=ax[1])

# mesh = plt.pcolormesh(xs, xs, cov)
# plt.xlabel("m, GeV")
# plt.ylabel("m, GeV")
# plt.gca().invert_yaxis()
# plt.title("Linear approximation of the covariance matrix")
# plt.colorbar(mesh, ax=plt.gca())


if __name__ == "__main__":
    plt.show()
