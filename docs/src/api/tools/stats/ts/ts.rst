Test statistic
==============

.. automodule:: sigcorr.tools.stats.ts

.. toctree::
   /api/tools/stats/ts/upcross
   /api/tools/stats/ts/euler_number
   /api/tools/stats/ts/sigcov_linear_model
