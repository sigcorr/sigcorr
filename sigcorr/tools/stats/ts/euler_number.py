import numpy as np
import scipy as sp
from scipy import stats

from sigcorr.tools.stats.ts.upcross import propagate_upcross


def avg_ts_euler_number_propagated(local_ts: np.ndarray, noise_dof: np.ndarray,
                                   ref_ts: np.ndarray, ref_euler_num: np.ndarray) -> np.ndarray:
    """Propagate average Euler characteristic (number) of overflow sets of the test statistic curve between thresholds.

    The overflow set is a set of points of a curve with values exceeding some threshold. The Euler characteristic of such a set
    is an important characteristic of the curve.

    For one particular curve one can use :py:mod:`~sigcorr.tools.euler_number` to estimate the Euler characteristic. When done
    on many curves from one statistical model, the average Euler characteristic can be estimated for some threshold.

    It is possible to propagate this estimate between thresholds, because for the curve the average Euler characteristic
    is closely related to the average number of up-crossings that is possible propagate:
    :py:func:`~sigcorr.tools.stats.ts.upcross.propagate_upcross`.

    For any model, therefore, when the average Euler characteristic is known (``ref_euler_num``)
    at some threshold (``ref_ts``), it is possible to predict the average Euler characteristic
    for any other threhold ``local_ts``.

    Parameters
    ----------
        local_ts : np.ndarray
            Target test statistic threshold to predict Euler characteristic for.
        noise_dof : np.ndarray
            Number of degrees of freedom of the test statistic distribution.
        ref_ts : np.ndarray
            Reference threshold at which the average Euler characteristic is known.
        ref_euler_num : np.ndarray
            Average Euler characteristic at the reference level ``ref_ts``.

    Returns
    -------
        average_euler_number : np.ndarray
            Average Euler characteristic computed at thresholds ``local_ts``.

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.ts.euler_number import avg_ts_euler_number_propagated

        reference_threshold = 2.1
        reference_avg_euler_num = 3.23
        noise_dof = 2
        target_thresholds = np.array([2,3,4,5.])
        avg_euler_numbers = avg_ts_euler_number_propagated(target_thresholds, noise_dof,
                                                           reference_threshold, reference_avg_euler_num)
        # array([3.32263766, 2.41805846, 1.67257808, 1.12452299])

    .. seealso::

        :py:mod:`sigcorr.tools.upcross`, :py:func:`sigcorr.tools.stats.ts.upcross.propagate_upcross`

        :py:func:`sigcorr.tools.stats.ts.euler_number.avg_ts_euler_number`
    """
    ref_upcross = ref_euler_num - (1-sp.stats.chi2(df=noise_dof).cdf(ref_ts))
    propagated_upcross = propagate_upcross(ref_ts, ref_upcross, noise_dof, local_ts)
    return avg_ts_euler_number(local_ts, noise_dof, propagated_upcross)


def avg_ts_euler_number(local_ts: np.ndarray, noise_dof: int, upcross: np.ndarray) -> np.ndarray:
    r"""Estimate average Euler characteristic of the overflow set of the test statistic from the
    average number of up-crossings.

    When doing a one dimensional likelihood scan, a test statistic can be constructed for each point of the scanning
    grid. In the asymptotic regime every point of this curve follows a chi-squared distribution with ``noise_dof``
    degrees of freedom.

    An important characteristic of such a curve is the Euler characteristic of the set of points of this curve above some
    threshold (overflow set), and it is a function of the threshold.

    The average Euler characteristic for a curve above threshold consists of two components:

        * average number of up-crossings of the threshold,
        * a boundary term that accounts for the up-crossing that we didn't notice because it happend behind
          the left edge of the scanning region while still contributing to the peak that is observed in the region.

    The boundary term is just a probability to observe the edge point above the threshold, or alternatively
    it is the p-value of the test statistic that can be computed from the cdf of the chi-squared distribution.

    We can, therefore, write more formally:

    .. math::

        <\text{Euler number}(c)> = P(TS > c | TS \sim \chi^2_s) + <\text{up-crossings}(c)>,

    where :math:`s` is the number of degrees of freedom of the test statistic (``noise_dof``).

    Parameters
    ----------
        local_ts : np.ndarray
            A test statistic threshold at which the average number of up-crossings is known.
        noise_dof : int
            Number of degrees of freedom of the test statistic distribution.
        upcross : np.ndarray
            Average number of up-crossings of the threshold ``local_ts``.

    Returns
    -------
        euler_number : np.ndarray
            Euler characteristic corresponding to the measured average number of up-crossings at the given threshold.

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.ts.euler_number import avg_ts_euler_number

        target_thresholds = np.array([1, 2, 3, 4, 5])
        noise_dof = 3
        avg_upcross = 4.2
        avg_euler_numbers = avg_ts_euler_number(target_thresholds, noise_dof, avg_upcross)
        # array([5.00125196, 4.7724067 , 4.59162518, 4.46146413, 4.37179714])

    .. seealso::

        :py:mod:`sigcorr.tools.upcross`, :py:mod:`sigcorr.tools.stats.ts.upcross`
    """
    return (1-sp.stats.chi2(df=noise_dof).cdf(local_ts)) + upcross
