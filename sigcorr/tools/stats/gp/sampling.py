from typing import Callable

import numpy as np
import scipy as sp
from scipy import stats


def draw_mv_gp(cov: np.ndarray, num: int) -> np.ndarray:
    """Draw samples from 0-mean multivariate Gaussian distribution with known
    covariance matrix.

    Use :py:class:`scipy.stats.multivariate_normal` as a sampler.

    Parameters
    ----------
        cov : np.ndarray
            Covariance matrix of the multivariate Gaussian distribution.
        num : int
            Number of samples.

    Returns
    -------
        samples : np.ndarray
            ``num`` samples from the 0-mean multivariate Gaussian distribution with covariance ``cov``.
            Shape is ``(num_samples, cov.shape[0])``

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.gp.sampling import draw_mv_gp

        cov = np.array([[1, 0.1],
                        [0.1, 1]])
        num_samples = 10
        samples = draw_mv_gp(cov, num_samples)
        # samples.shape = (10, 2)

    .. seealso::
        :py:func:`sp.stats.multivariate_normal`
    """
    dist = get_mv_dist(cov)
    return draw_gp_from_dist(dist, num)


def get_mv_dist(cov: np.ndarray) -> sp.stats.rv_continuous:
    """Get SciPy instance of the 0-mean multivariate Gaussian distribution with known covariance.

    Parameters
    ----------
        cov : np.ndarray
            Covariance matrix.

    Returns
    -------
        dist : sp.stats.rv_continuous
            Instance of the multivariate Gaussian distribution with 0-mean and covariance ``cov``.

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.gp.sampling import get_mv_dist

        cov = np.array([[1, 0.1],
                        [0.1, 1]])
        dist = get_mv_dist(cov)
        samples = dist.rvs(10)
        # samples.shape = (10, 2)

    .. seealso::
        :py:func:`sp.stats.multivariate_normal`
    """
    return sp.stats.multivariate_normal(mean=None, cov=cov, allow_singular=True)


def draw_gp_svd(cov: np.ndarray, num: int) -> np.ndarray:
    """Draw samples from the 0-mean multivariate Gaussian distribution using
    SVD decomposition for the covariance.

    Parameters
    ----------
        cov : np.ndarray
            Covariance matrix.
        num : int
            Number of samples.

    Returns
    -------
        samples : np.ndarray
            ``num`` samples from the 0-mean multivariate Gaussian distribution with covariance ``cov``.
            Shaped as ``(num_samples, cov.shape[0])``

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.gp.sampling import draw_gp_svd

        cov = np.array([[1, 0.1],
                        [0.1, 1]])
        num_samples = 10
        samples = draw_gp_svd(cov, num_samples)
        # samples.shape = (10, 2)

    .. seealso::
        :py:func:`np.linalg.svd`
    """
    sqrt_cov = get_svd_sqrtcov(cov)
    return draw_gp_from_sqrtcov(sqrt_cov, num)


def get_svd_sqrtcov(cov):
    """Get square root of the covariance matrix using SVD decomposition.

    Parameters
    ----------
        cov : np.ndarray
            Covariance matrix

    Returns
    -------
        sqrtcov : np.ndarray
            Square root of the covariance matrix from the SVD decomposition.

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.gp.sampling import get_svd_sqrtcov

        cov = np.array([[1, 0.1],
                        [0.1, 1]])
        sqrtcov = get_svd_sqrtcov(cov)
        cov_restored = np.matmul(sqrtcov, sqrtcov.T)
        # np.isclose(cov_restored, cov)

    .. seealso::
        :py:func:`np.linalg.svd`
    """
    u, s, _ = np.linalg.svd(cov)
    sqrt_cov = np.sqrt(s).reshape(1, -1)*u
    return sqrt_cov


def draw_gp_chol(cov, num):
    """Draw samples from the 0-mean multivariate Gaussian distribution using
    Cholesky decomposition for the covariance.

    Parameters
    ----------
        cov : np.ndarray
            Covariance matrix.
        num : int
            Number of samples.

    Returns
    -------
        samples : np.ndarray
            ``num`` samples from 0-mean the multivariate Gaussian distribution with covariance ``cov``.
            Shaped as ``(num_samples, cov.shape[0])``

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.gp.sampling import draw_gp_chol

        cov = np.array([[1, 0.1],
                        [0.1, 1]])
        num_samples = 10
        samples = draw_gp_chol(cov, num_samples)
        # samples.shape = (10, 2)

    .. seealso::
        :py:func:`np.linalg.cholesky`
    """
    sqrt_cov = get_chol_sqrtcov(cov)
    return draw_gp_from_sqrtcov(sqrt_cov, num)


def get_chol_sqrtcov(cov: np.ndarray) -> np.ndarray:
    """Get square root of the covariance matrix using Cholesky decomposition.

    Parameters
    ----------
        cov : np.ndarray
            Covariance matrix

    Returns
    -------
        sqrtcov : np.ndarray
            Square root of the covariance matrix from the Cholesky decomposition.

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.gp.sampling import get_chol_sqrtcov

        cov = np.array([[1, 0.1],
                        [0.1, 1]])
        sqrtcov = get_chol_sqrtcov(cov)
        cov_restored = np.matmul(sqrtcov, sqrtcov.T)
        # np.isclose(cov_restored, cov)

    .. seealso::
        :py:func:`np.linalg.cholesky`
    """
    sqrt_cov = np.linalg.cholesky(cov)
    return sqrt_cov


def draw_gp_from_sqrtcov(sqrtcov: np.ndarray, num: int) -> np.ndarray:
    """Draw samples from 0-mean multivariate Gaussian distribution with known square root of
    the covariance matrix.

    Parameters
    ----------
        sqrtcov : np.ndarray
            Square root of the covariance matrix.
        num : int
            Number of samples.

    Returns
    -------
        samples : np.ndarray
            ``num`` samples from 0-mean the multivariate Gaussian distribution with covariance ``sqrtcov*sqrtcov.T``.
            Shaped as ``(num_samples, sqrtcov.shape[0])``

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.gp.sampling import draw_gp_from_sqrtcov

        sqrtcov = np.array([[-0.1, -0.8],
                            [-0.1, 0.8]])
        num_samples = 10
        samples = draw_gp_from_sqrtcov(sqrtcov, num_samples)
        # samples.shape = (10, 2)
    """
    return np.matmul(sqrtcov, sp.stats.norm().rvs((num, sqrtcov.shape[0])).T).T


def draw_gp_from_dist(dist: sp.stats.rv_continuous, num: int) -> np.ndarray:
    """Draw samples from SciPy distribution.

    Parameters
    ----------
        dist : sp.stats.rv_continuous
            SciPy distribution.
        num : int
            Number of samples.

    Returns
    -------
        samples : np.ndarray
            ``num`` samples from the SciPy distribution ``dist``.

    Examples
    --------

    .. testcode::

        import numpy as np
        import scipy as sp
        from scipy import stats
        from sigcorr.tools.stats.gp.sampling import draw_gp_from_dist

        mu = np.array([0, 1.])
        cov = np.array([[1, 0.1],
                        [0.1, 1]])
        dist = sp.stats.multivariate_normal(mean=mu, cov=cov)

        num_samples = 10
        samples = draw_gp_from_dist(dist, num_samples)
        # samples.shape = (10, 2)
    """
    return dist.rvs(num)


def get_covariance_from_kernel(kernel: Callable, xs: np.ndarray) -> np.ndarray:
    r"""Get covariance matrix from the covariance kernel evaluated on a grid.

    Parameters
    ----------
        kernel : Callable
            Covariance kernel. A function: :math:`(x_i, x_j) \rightarrow cov_{ij}`.
        xs : np.ndarray
            Grid of points on which to evaluate the kernel.

    Returns
    -------
        covariance_matrix : np.ndarray
            Covariance matrix from the covariance kernel ``kernel`` on the grid ``xs``.

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.gp.sampling import get_covariance_from_kernel

        def kernel(x1, x2):
            return np.exp(-(x1 - x2)**2/2)

        xs = np.linspace(0, 1, 10)
        cov = get_covariance_from_kernel(kernel, xs)
        # cov.shape = (10, 10)
    """
    return kernel(xs[np.newaxis, :], xs[:, np.newaxis])


def gibbs_kernel(l, x1, x2):
    return np.sqrt(2*l(x1)*l(x2)/(l(x1)**2 + l(x2)**2))*np.exp(-(x1-x2)**2/(l(x1)**2 + l(x2)**2))
