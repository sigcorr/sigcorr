Model
=====

.. autoclass:: sigcorr.models.model.AbstractModel
   :members:
   :special-members: __init__

Additional protocols
---------------------

.. autoclass:: sigcorr.models.model.LinearApproximable
   :members:
