import h5py
import numpy as np
import matplotlib.pyplot as plt
from sigcorr.tools.stats.utils import sig2pval
from sigcorr.mapreduce.file import h5_batch_mapreduce
from sigcorr.mapreduce.gp import gp_batch_mapreduce
from sigcorr.tools.stats.gp.sampling import get_svd_sqrtcov
from sigcorr.mapreduce.map_reducers import ChainCalc
from sigcorr.mapreduce.map_reducers import SigsCalc
from sigcorr.mapreduce.map_reducers import MathCalc
from sigcorr.mapreduce.map_reducers import OverflowsCalc
from sigcorr.mapreduce.map_reducers import EulerNumberCalc
from sigcorr.mapreduce.map_reducers import BatchStats2Reduce
from sigcorr.mapreduce.map_reducers import BatchStats1Reduce
from sigcorr.tools.stats.gp.euler_number import GPEulerNumberPropagator
from sigcorr.tools.utils import get_last_from_iter


with h5py.File("../data/Asimov2D/hyy_tutorial.h5", "r") as fin:
    xs = fin["scan_xs"][...]


pipeline = h5_batch_mapreduce("../data/Asimov2D/hyy_tutorial.h5", ["b_loglikes", "sb_loglikes", "sb_params"],
                              10,  # batch size
                              ChainCalc([SigsCalc(), MathCalc(lambda b: b.reshape(b.shape[0], -1))]),
                              BatchStats2Reduce())
bs, _ = get_last_from_iter(pipeline)
cov = bs.get_corr(override_x=np.array([0.]))
sqrt_cov = get_svd_sqrtcov(cov)

local_sig_grid = np.arange(0, 3.05, 0.1)  # i.e. significances between 0 and 3 with step 0.1
local_p = sig2pval(local_sig_grid)

p_global_bs, _ = get_last_from_iter(
    gp_batch_mapreduce(None,  # we provide sqrt cov later
                       10_000,  # num samples
                       250,  # batch size
                       xs.shape[:-1],
                       OverflowsCalc(local_sig_grid),
                       BatchStats1Reduce(),
                       sqrt_cov=sqrt_cov)
)
tf_gp = p_global_bs.get_mean()/local_p
tf_gp_err = p_global_bs.get_stat_err()/local_p

ref_sigs = np.array([0.7, 1.])

avg_euler_bs1, _ = get_last_from_iter(
    gp_batch_mapreduce(None,
                       5000,  # num samples
                       250,  # batch size
                       xs.shape[:-1],
                       EulerNumberCalc(ref_sigs[0]),
                       BatchStats1Reduce(),
                       sqrt_cov=sqrt_cov)
)
avg_euler_bs2, _ = get_last_from_iter(
    gp_batch_mapreduce(None,
                       5000,  # num samples
                       250,  # batch size
                       xs.shape[:-1],
                       EulerNumberCalc(ref_sigs[1]),
                       BatchStats1Reduce(),
                       sqrt_cov=sqrt_cov)
)

avg_euler_nums = np.array([avg_euler_bs1.get_mean()[0], avg_euler_bs2.get_mean()[0]])

p_global_upper = GPEulerNumberPropagator(ref_sigs, avg_euler_nums).calc(local_sig_grid)
tf_upper = p_global_upper/local_p

plt.errorbar(local_sig_grid, tf_gp, yerr=tf_gp_err, label="TF from GP toys", color="orange", marker=".", capsize=2)
plt.plot(local_sig_grid, tf_upper, label="VG TF upper bound")
plt.scatter([ref_sigs], [avg_euler_nums/sig2pval(ref_sigs)], marker="x", s=18, color="black",
            zorder=10, label="VG reference point")
plt.xlabel(r'$\mathrm{Z}_{local}$')
plt.ylabel("TF")
plt.title("Trials factor comparison")
plt.legend()


if __name__ == "__main__":
    plt.show()
