Euler number
============

.. automodule:: sigcorr.tools.stats.gp.euler_number
   :undoc-members:
   :members: GPEulerNumberPropagator, avg_gp_euler_number
