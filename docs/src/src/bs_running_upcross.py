import numpy as np
import h5py
import matplotlib.pyplot as plt
from sigcorr.mapreduce.file import h5_batch_mapreduce
from sigcorr.mapreduce.gp import gp_batch_mapreduce
from sigcorr.tools.utils import get_last_from_iter
from sigcorr.mapreduce.map_reducers import SigsCalc
from sigcorr.mapreduce.map_reducers import ChainCalc
from sigcorr.mapreduce.map_reducers import MathCalc
from sigcorr.mapreduce.map_reducers import UpcrossCalc
from sigcorr.mapreduce.map_reducers import BatchStats1Reduce
from sigcorr.mapreduce.map_reducers import BatchStats2Reduce
from sigcorr.tools.stats.gp.upcross import gp_upcross_at_level


with h5py.File("../data/3K1D/hyy_tutorial.h5", "r") as fin:
    xs = fin["scan_xs"][...].ravel()


bs, _ = get_last_from_iter(h5_batch_mapreduce("../data/3K1D/hyy_tutorial.h5",
                                              ["b_loglikes", "sb_loglikes", "sb_params"],
                                              200,
                                              SigsCalc(),
                                              BatchStats2Reduce()))
cov = bs.get_cov()

nums = []
avg_upcross = []
avg_err = []
pipeline = gp_batch_mapreduce(cov,
                              100_000,  # num samples
                              500,  # batch size
                              xs.shape,  # sample shape
                              ChainCalc([MathCalc(np.square), UpcrossCalc(1.)]),  # up-crossings at level 1.
                              BatchStats1Reduce())
for intermediate_bs, num_processed in pipeline:
    corrected_avg_upcross = intermediate_bs.get_mean()[0]*intermediate_bs.n/num_processed
    avg_upcross.append(corrected_avg_upcross)
    corrected_std_upcross = (np.sum(intermediate_bs.xx)*intermediate_bs.n/num_processed -
                             corrected_avg_upcross**2) / np.sqrt(num_processed)
    avg_err.append(corrected_std_upcross)
    nums.append(num_processed)


plt.errorbar(nums, avg_upcross, yerr=avg_err, label="avg number of up-crossings", capsize=2, elinewidth=0)
plt.xlabel("Number of samples")
plt.ylabel("Avg up-crossings at level 1.")
plt.title("Convergence of the avg. number of up-crossings")
plt.ylim([2.14, 2.2])


if __name__ == "__main__":
    plt.show()
