Linear approximation for |project| model
========================================

.. automodule:: sigcorr.linear_approx_sigcov
   :undoc-members:
   :members: get_linear_approx_sigcov_for_model
