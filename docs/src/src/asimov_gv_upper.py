import numpy as np
import h5py
import matplotlib.pyplot as plt

from sigcorr.tools.stats.utils import get_delta_sigs
from sigcorr.tools.stats.utils import sig2pval

from sigcorr.tools.stats.batch_stats import BatchStats2
from sigcorr.tools.stats.gp.upcross import gp_upcross_at_level
from sigcorr.tools.stats.gp.euler_number import avg_gp_euler_number
from sigcorr.tools.stats.gp.euler_number import GPEulerNumberPropagator

from sigcorr.mapreduce.gp import gp_batch_mapreduce
from sigcorr.mapreduce.map_reducers import OverflowsCalc
from sigcorr.mapreduce.map_reducers import UpcrossCalc
from sigcorr.mapreduce.map_reducers import BatchStats1Reduce
from sigcorr.tools.utils import get_last_from_iter


bf_res = None
with h5py.File("../data/Asimov1D/hyy_tutorial.h5", "r") as fin:
    fields = ["b_loglikes", "sb_loglikes", "sb_params"]
    bf_res = {k: fin[k][:61, ...] for k in fields}
    xs = fin["scan_xs"][...].ravel()


sigs = get_delta_sigs(bf_res["b_loglikes"], bf_res["sb_loglikes"], bf_res["sb_params"][..., 0])

num_samples = sigs.shape[0]
sample_dim = sigs.shape[1]
bs = BatchStats2(num_samples, sample_dim)
bs.push(sigs)
cov = bs.get_corr(override_x=np.array([0.]))
cov_err = bs.get_corr_stat_err(override_x=np.array([0.]))


local_sig_grid = np.arange(0, 3.05, 0.1)  # i.e. significances between 0 and 3 with step 0.1

p_global_bs, _ = get_last_from_iter(
    gp_batch_mapreduce(cov,
                       1_000_000,  # num samples
                       2000,  # batch size
                       xs.shape,
                       OverflowsCalc(local_sig_grid),
                       BatchStats1Reduce())
)
p_global = p_global_bs.get_mean()
p_global_err = p_global_bs.get_stat_err()

local_p = sig2pval(local_sig_grid)
tf_gp = p_global/local_p
tf_gp_err = p_global_err/local_p


ref_sigs = np.array([0.7])

pipeline = gp_batch_mapreduce(cov,
                              1_000_000,  # num samples
                              2000,  # batch size
                              xs.shape,
                              UpcrossCalc(ref_sigs[0]),
                              BatchStats1Reduce())
avg_upcross_bs, _ = get_last_from_iter(pipeline)
avg_euler_num = avg_gp_euler_number(ref_sigs, avg_upcross_bs.get_mean())
p_global_upper = GPEulerNumberPropagator(ref_sigs, avg_euler_num).calc(local_sig_grid)

gv_tf_upper_bound_from_samples = p_global_upper/local_p


avg_upcross = np.array([gp_upcross_at_level(xs, cov, sig) for sig in local_sig_grid])
avg_euler_numbers = avg_gp_euler_number(local_sig_grid, avg_upcross)
gv_tf_upper_bound_from_cov = avg_euler_numbers/local_p


plt.plot(local_sig_grid, gv_tf_upper_bound_from_samples, label="GV TF upper bound from samples")
plt.plot(local_sig_grid, gv_tf_upper_bound_from_cov, label="GV TF upper bound from cov.", color="red", ls="--")
plt.errorbar(local_sig_grid, tf_gp, yerr=tf_gp_err, label="TF from GP toys", color="orange", marker=".", capsize=2)
plt.scatter(ref_sigs, avg_euler_num/sig2pval(ref_sigs), marker="x", s=18, color="black",
            zorder=10, label="GV reference point")
plt.xlabel(r'$\mathrm{Z}_{local}$')
plt.ylabel("TF")
plt.legend()
plt.title("Trials factor comparison")
plt.ylim([0, 25])


if __name__ == "__main__":
    plt.show()
