API
===

.. toctree::
   /api/model
   /api/fitter
   /api/linear_approx_sigcov
   /api/mapreduce/mapreduce
   /api/tools/tools
