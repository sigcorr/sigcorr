import sys

from typing import Tuple
from typing import Optional
from datetime import datetime
from multiprocessing import Process
from multiprocessing import Queue

import numpy as np
from numpy import random
from tqdm import tqdm

from sigcorr.config import CFG
from sigcorr.tools.utils import produce_batch_ranges
from sigcorr.tools.stats.gp.sampling import get_svd_sqrtcov
from sigcorr.tools.stats.gp.sampling import draw_gp_from_sqrtcov
from sigcorr.mapreduce.map_reducers import Calculator
from sigcorr.mapreduce.map_reducers import Reducer


def gp_batch_mapreduce(cov: Optional[np.ndarray], n: int, batchsize: int, sample_dim: Tuple[int, ...],
                       mapper: Calculator, reducer: Reducer, sqrt_cov: Optional[np.ndarray] = None):
    """
    Generate ``n`` multivariate Gaussian samples from the covariance matrix ``corrmat``,
    reshape each sample according to ``sample_dim``, apply ``mapper`` to batches of size ``batchsize``,
    then aggregate processed batches with ``reducer``.

    Number of parallel processes is defined in the configuration: ``sigcorr.config.CFG.FITTER.bfit_poolsize``.

    You can either change number of processes by setting the new number explicitly
    in the code or use environment variables at runtime:

    .. code::

        SIGCORR_FITTER_bfit_poolsize=10 python3 your_script.py

    Parameters
    ----------
        cov : Optional[np.ndarray]
            2d covariance matrix for the multivariate Gaussian. If ``sqrt_cov`` is provided, this argument is not used
        n : int
            number of samples to be generated
        batchsize : int
            samples are split into batches of this size
        sample_dim : Tuple[int, ...]
            flat vector sampled from ``corrmat`` will be reshaped to a new shape:``sample_dim``.
        mapper : Calculator
            instance of the :py:class:`~sigcorr.mapreduce.map_reducers.Calculator` that processes batches
        reducer : Reducer
            instance of the :py:class:`~sigcorr.mapreduce.map_reducers.Reducer` that aggregates batches
        sqrt_cov : Optional[np.ndarray]
            provide square root of the covariance. If provided, ``cov`` is not used

    Returns
    -------
        result : Iterator[Tuple]
            Intermediate results of the aggregation yielded as tuple: ``(intermediate_result, num_processed)``.

            The last element of the iterator is the final result of the processing.

    Examples
    --------

    Example of computing the average of the Gaussian samples:

    .. testcode::

        import numpy as np
        from sigcorr.mapreduce.gp import gp_batch_mapreduce
        from sigcorr.tools.utils import get_last_from_iter
        from sigcorr.mapreduce.map_reducers import MathCalc
        from sigcorr.mapreduce.map_reducers import BatchStats1Reduce

        cov = np.eye(3) + 0.1
        result_iterator = gp_batch_mapreduce(cov,
                                             100,
                                             10,
                                             (3, ),
                                             MathCalc(lambda x: x),  # noop
                                             BatchStats1Reduce())
        resulting_bs, num_processed = get_last_from_iter(result_iterator)
        resulting_bs.get_mean()

    .. seealso::
        :doc:`/tutorial/batch_multiprocessing` tutorial, :py:mod:`sigcorr.map_reducers`,
        :py:class:`sigcorr.mapreduce.map_reducers.MathCalc`,
        :py:class:`sigcorr.mapreduce.map_reducers.BatchStats1Reduce`

        :doc:`/tutorial/fitting` tutorial to learn more about the configuration.

        Helper: :py:func:`sigcorr.tools.utils.get_last_from_iter`
    """
    in_q = Queue()
    out_q = Queue()
    ref_seed = int(datetime.now().timestamp())//2
    sqrt_corrmat = get_svd_sqrtcov(cov) if sqrt_cov is None else sqrt_cov
    processes = [Process(target=_gp_process_batches,
                         args=(in_q, ref_seed+i, sqrt_corrmat, sample_dim, mapper, out_q))
                 for i in range(CFG.FITTER.bfit_pool_size)]
    [p.start() for p in processes]
    num_batches = 0
    for brange in produce_batch_ranges(n, batchsize):
        num_batches += 1
        in_q.put(brange.stop - brange.start)
    [in_q.put(None) for _ in processes]
    results_iterator = _gp_batch_iterresults(out_q, batchsize, num_batches)
    try:
        yield from reducer.reduce_batches(results_iterator, n)
        [p.join() for p in processes]
    finally:
        [p.kill() for p in processes]


def _gp_process_batches(q_in, seed, sqrt_cov, sample_dim, mapper, q_out):
    np.random.seed(seed)
    while True:
        size = q_in.get()
        if size is None:
            return
        samples = draw_gp_from_sqrtcov(sqrt_cov, size)
        if sample_dim is not None:
            samples = samples.reshape(-1, *sample_dim)
        res = mapper.process_batch(samples)
        q_out.put(res)


def _gp_batch_iterresults(batch_res_q, approx_batchsize, num_batches):
    for _ in tqdm(range(num_batches),
                  total=num_batches, unit_scale=approx_batchsize, file=sys.stderr):
        one_batch_res = batch_res_q.get()
        yield one_batch_res
