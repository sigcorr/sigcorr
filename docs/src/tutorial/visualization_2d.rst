Visualization 2D
================

The model we have chosen to demonstrate a 2D signal parameter scan is similar to the Hyy model, however, in addition to the signal location, the signal width is
scanned. Moreover, the Asimov model is special because it produces background samples with carefully chosen fluctuations.
There are 61 points in the sampling grid, and there are 61 background samples containing a :math:`1\sigma`
fluctuation of 1 bin while the rest of the bins follow the smooth background template.
The choice of the model doesn't make any difference for the purpose of visualization, however, this little explanation
can help understand the patterns on the plots below if they surprise the reader :-).

We use :py:class:`~sigcorr.models.hyy2d.Hyy2DAsimov` for the model,
:src:`grids/hyy-common.dat` for the sampling grid, and :src:`grids/hyy2d_dense-scan.dat` for the scanning grid.
There are 61 fitted Asimov samples from this model stored in :toy_data:`Asimov2D/hyy_tutorial.h5`.

The 2D parameter scan can be effectively reduced to 1D when it comes to significance curves and their
correlation structure. In this section we will demonstrate some specifics of the batch multiprocessing related
to the transition between one-dimensional and multi-dimensional scans.


Significance surface
--------------------


In the 2D scan, a significance curve becomes a significance surface. Here is an example of how to read the samples
and build the significance curves with :py:func:`~sigcorr.tools.stats.utils.get_delta_sigs`:

.. code::

    import h5py
    from sigcorr.tools.stats.utils import get_delta_sigs

    bf_res = None
    with h5py.File("../data/Asimov2D/hyy_tutorial.h5", "r") as fin:
        fields = ["b_loglikes", "sb_loglikes", "sb_params"]
        bf_res = {k: fin[k][:61, ...] for k in fields}
        xs = fin["scan_xs"][...]

    sigs = get_delta_sigs(bf_res["b_loglikes"], bf_res["sb_loglikes"], bf_res["sb_params"][..., 0])

Notice, we do not ``ravel`` (flatten) the ``xs`` array anymore. In 2D it is sometimes important to distinguish between
the axes of the scanning grid. Here we see a significance surface for an Asimov background dataset with a fluctation in
the mass bin near 135 GeV.

.. plot:: src/sigcurve2d.py


Covariance matrix
-----------------

The covariance matrix for a 2D signal scan is 4-dimensional. On the other hand, for the covariance between two points
it doesn't matter whether they are part of surfaces or lines, they are just two random variables with some degrees of
correlation. Therefore, we
will unwrap 2D surfaces into 1D lines (arrays) of significances and then apply the 1D approach for computing the
covariance matrix.

Similarly to :ref:`the 1D calculation <tutorial/batch_multiprocessing:Estimating the covariance matrix: BatchStats multiprocessing>`
we use :py:class:`~sigcorr.mapreduce.map_reducers.SigsCalc` for the significance surface calculation in combination with
:py:func:`~sigcorr.mapreduce.map_reducers.BatchStats2Reduce` for the covariance calculation. We use, however, 
:py:class:`~sigcorr.mapreduce.map_reducers.MathCalc` chained after
the :py:class:`~sigcorr.mapreduce.map_reducers.SigsCalc` specifically to unwrap the significance surface into a line.

.. NOTE:: 
    While computing the covariance from the set of Asimov background samples, it is a good practice to use known properies
    of the significance curves. We know that the local significance follows the standard normal distribution with mean 0
    and variance 1. By overriding the ``x`` we are forcing the former condition, and by computing the correlation instead
    of the covariance we impose the latter.

.. code::

    import h5py
    from sigcorr.mapreduce.file import h5_batch_mapreduce
    from sigcorr.mapreduce.map_reducers import ChainCalc
    from sigcorr.mapreduce.map_reducers import SigsCalc
    from sigcorr.mapreduce.map_reducers import MathCalc
    from sigcorr.mapreduce.map_reducers import BatchStats2Reduce
    from sigcorr.tools.utils import get_last_from_iter

    ...
    pipeline = h5_batch_mapreduce("../data/Asimov2D/hyy_tutorial.h5", ["b_loglikes", "sb_loglikes", "sb_params"],
                                  10,  # batch size
                                  ChainCalc([SigsCalc(), MathCalc(lambda b: b.reshape(b.shape[0], -1))]),
                                  BatchStats2Reduce())
    bs, _ = get_last_from_iter(pipeline)
    cov = bs.get_corr(override_x=np.array([0.]))

Example of the covariance with unwrapped scans of mass-width grids on both axes:

.. plot:: src/bs_covmat2d.py


Trials factor from GP toys in 2D
--------------------------------


Let's use the covariance matrix computed above to estimate trials factors for the 2D Hyy model.
This calculation repeats algorithmically
:ref:`the 1D calculation <tutorial/visualization_1d:Trials factor from GP toys>`, however, this time it is performed
with batch multiprocessing.

We sample significance surfaces from the GP covariance
with :py:func:`~sigcorr.mapreduce.gp.gp_batch_mapreduce`. A convenient detail here is that we compute the square root
of the covariance outside :py:func:`~sigcorr.mapreduce.gp.gp_batch_mapreduce`, which will be useful later when
we will need to sample from the same covariance multiple times.

We then use :py:class:`~sigcorr.mapreduce.map_reducers.OverflowsCalc` to count how
many of the significance surfaces exceed
a range of local significances (``local_sig_grid``) and aggregate the counts with
:py:class:`~sigcorr.mapreduce.map_reducers.BatchStats1Reduce`.


.. code::

    from sigcorr.tools.stats.utils import sig2pval
    from sigcorr.mapreduce.gp import gp_batch_mapreduce
    from sigcorr.tools.stats.gp.sampling import get_svd_sqrtcov
    from sigcorr.mapreduce.map_reducers import ChainCalc
    from sigcorr.mapreduce.map_reducers import SigsCalc
    from sigcorr.mapreduce.map_reducers import OverflowsCalc
    from sigcorr.mapreduce.map_reducers import BatchStats1Reduce
    from sigcorr.tools.utils import get_last_from_iter

    ...
    sqrt_cov = get_svd_sqrtcov(cov)

    local_sig_grid = np.arange(0, 3.05, 0.1)  # i.e. significances between 0 and 3 with step 0.1
    local_p = sig2pval(local_sig_grid)

    p_global_bs, _ = get_last_from_iter(
        gp_batch_mapreduce(None,
                           10_000,  # num samples
                           250,  # batch size
                           xs.shape[:-1],
                           OverflowsCalc(local_sig_grid),
                           BatchStats1Reduce(),
                           sqrt_cov=sqrt_cov)
    )
    tf_gp = p_global_bs.get_mean()/local_p
    tf_gp_err = p_global_bs.get_stat_err()/local_p

.. plot:: src/tf_2d.py


The Vitells and Gross upper bound on the trials factor
------------------------------------------------------


We will also use GP samples to estimate the average Euler characteristic at 2 local significance thresholds (0.7 and 1).
Here precomputed ``sqrt_cov`` from the previous step comes in handy.

Two thresholds are enough to propagate the average Euler characteristic to other significance levels and therefore to set
the Vitells and Gross upper bound on the trials factor for the 2D scan [1]_.

We use :py:class:`~sigcorr.mapreduce.map_reducers.EulerNumberCalc` to calculate the charcteristic number of every sample
in the batch, and then we aggregate the computed Euler characteristics
with :py:class:`~sigcorr.mapreduce.map_reducers.BatchStats1Reduce` to get the average.
The logic is similar to :ref:`the 1D calculation <tutorial/visualization_1d:Gross and Vitells upper bound from up-crossings>`
, but again, wrapped into batch multiprocessing and now using
:py:func:`~sigcorr.tools.stats.gp.euler_number.GPEulerNumberPropagator` for two thresholds instead of one.


.. code::

    from sigcorr.mapreduce.gp import gp_batch_mapreduce
    from sigcorr.mapreduce.map_reducers import ChainCalc
    from sigcorr.mapreduce.map_reducers import SigsCalc
    from sigcorr.mapreduce.map_reducers import EulerNumberCalc
    from sigcorr.mapreduce.map_reducers import BatchStats1Reduce
    from sigcorr.tools.stats.gp.euler_number import GPEulerNumberPropagator
    from sigcorr.tools.utils import get_last_from_iter

    ...
    ref_sigs = np.array([0.7, 1.])

    avg_euler_bs1, _ = get_last_from_iter(
        gp_batch_mapreduce(None,
                           5000,  # num samples
                           250,  # batch size
                           xs.shape[:-1],
                           EulerNumberCalc(ref_sigs[0]),
                           BatchStats1Reduce(),
                           sqrt_cov=sqrt_cov)
    )
    avg_euler_bs2, _ = get_last_from_iter(
        gp_batch_mapreduce(None,
                           5000,  # num samples
                           250,  # batch size
                           xs.shape[:-1],
                           EulerNumberCalc(ref_sigs[1]),
                           BatchStats1Reduce(),
                           sqrt_cov=sqrt_cov)
    )

    avg_euler_nums = np.array([avg_euler_bs1.get_mean()[0], avg_euler_bs2.get_mean()[0]])

    p_global_upper = GPEulerNumberPropagator(ref_sigs, avg_euler_nums).calc(local_sig_grid)
    tf_upper = p_global_upper/local_p

.. plot:: src/tf_vg_upper_2d.py

References
==========

.. [1] O. Vitells and E. Gross, "Estimating the significance of a signal in a multi-dimensional search,"
       Astroparticle Physics, Volume 35, Issue 5, 2011, Pages 230-234, ISSN 0927-6505,
       https://doi.org/10.1016/j.astropartphys.2011.08.005
