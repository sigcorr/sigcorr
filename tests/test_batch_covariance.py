import unittest

import numpy as np
from numpy import random

from sigcorr.tools.stats.batch_stats import BatchStats2


ABS_DELTA = 1E-5


class TestBatchStats(unittest.TestCase):
    def setUp(self):
        np.random.seed(148)

    def test_batch_covariance(self):
        for _ in range(10):
            self._one_covariance_test()

    def _one_covariance_test(self):
        samples = np.random.random((8, 5))
        covcalc = BatchStats2(*samples.shape)
        covcalc.push(samples[:4])
        covcalc.push(samples[4:])
        cov, var = covcalc.get_cov(), covcalc.get_cov_var()
        samples_c = samples - samples.mean(axis=0).reshape(1, -1)
        true_partials = np.einsum("ki,kj->kij", samples_c, samples_c)
        true_cov, true_var = true_partials.mean(axis=0), true_partials.std(axis=0)**2
        self.assertAlmostEqual(np.max(np.absolute(true_cov - cov)), 0, delta=ABS_DELTA)
        self.assertAlmostEqual(np.max(np.absolute(true_var - var)), 0, delta=ABS_DELTA)
