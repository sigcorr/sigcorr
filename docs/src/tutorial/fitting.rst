Fitting the toys
================


We assume that you have completed the :doc:`/tutorial/model_definition` and the :doc:`/tutorial/grids` chapters
of the tutorial, and you have the ``HyyTutorial`` model defined in the ``hyy_tutorial.py`` module
of the :src:`sigcorr/models` package of |project| installed locally in the development mode.

If you decided to skip the previous chapters, just make a copy of :src:`sigcorr/models/hyy.py` and rename the
file and the class inside. Also, you can use this grid file :src:`grids/hyy-common.dat` for practicing.

We are going to use the :src:`sigcorr-run` script to generate toys from the model and to fit these toys with the b- and sb-
models. To enable the newly created ``HyyTutorial`` model we need to add an extra import to the header of the script
and extend ``MODELS`` list with the ``HyyTutorial`` class:


.. code::

    from sigcorr.models.hyy_tutorial import HyyTutorial

    MODELS = [..., HyyTutorial]


Basic run
---------


.. WARNING::
    Remember to ``source env.sh`` before running the fits.

Let's run the fits:

.. code:: bash

    sigcorr-run -g grids/hyy-common.dat -o tmp/hyy_tutorial.h5 -n 1000 HyyTutorial

During the run you will see a progress bar twice, once for b-fits and once for sb-fits.
Here is an example of such a progress bar for b-fits:

.. code:: bash

    90%|██████████████████  | 900/1000 [00:05<00:02, 739.70it/s, writer_qsize=0, workers_qsize=1]


What we see:

* ``900/1000`` - number of fits processed out of the total number of fits. We ran ``sigcorr-run`` for ``-n 1000``,
  so we requested ``1000`` toys, i.e. background fits (as in the example above). For sb-fits, the total number is
  the product of the number of toys and the number of points per signal scan (``scanning grid`` size).
* ``739 it/s`` - average performance of the fitter. How many fits (iterations) per second it produces.
* ``writer_qsize`` - number of processed batches waiting to be written to disk.
* ``workers_qsize`` - number of unprocessed batches waiting to be pulled and processed by workers.


There are more options available with :code:`sigcorr-run --help`. The most important ones are:

* ``-s`` - specify the ``scanning grid`` if it is different from the ``sampling grid`` (provided with ``-g``)
* ``-f``, ``--force`` - overwrite output file. Default behavior is to raise an exception if output file exists.


Settings, batches and pools
---------------------------


The :py:class:`~sigcorr.fitter.Fitter`, which ``sigcorr-run`` uses under the hood, produces MC samples, splits them
into batches and feeds them to workers that do likelihood fits. Then workers send results to the writer process that
writes to the output file.

By default, batch sizes and worker pool sizes are kept small, to be able to run on a laptop. However, on a powerful node
you might want to scale these up. You can override the default values for batch sizes and pool sizes using environment variables
(number of available CPUs is a good value to start with):

.. code:: bash

    export SIGCORR_FITTER_bfit_batchsize=300
    export SIGCORR_FITTER_bfit_pool_size=70

    export SIGCORR_FITTER_sbfit_batchsize=5
    export SIGCORR_FITTER_sbfit_pool_size=250

    sigcorr-run -g grids/hyy-common.dat -o tmp/hyy_tutorial.h5 -n 3000000 HyyTutorial


With this setup there will be 70 workers to process background fits, each worker will receive batches of 300 toys
and will perform 300 background fits. With 3M toys, each worker will receive on average 140 batches.

For the signal+background fits, however, the pool consists of 250 workers. One batch contains 5 toys, but each toy
requires 61 signal+background fits, therefore, one batch results in ~300 fits. On the average, each worker will process
2400 batches.


It is reasonable to set a larger batch size for the b-fits because there is a single fit per toy,
while the sb-fits might require a lower number of toys per batch to compensate for the scan of the signal grid. The rule of thumb would be
``bfit_batch_size * signal_grid_size = sbfit_batch_size``.


These are likely to be the most frequently used options. Additional options are availible in :src:`sigcorr/config.py`.


HDF5 file structure
-------------------

There are several parameters which define the shapes of the arrays stored in the resulting
HDF5 file:

* number of fitted samples
* number of background only model params
* number of signal+background model params
* sampling grid dimensionality and the number of points
* scanning grid dimensionality and the number of points

Then the file contains:

    A dataset of generated background samples:

    * ``bg_samples``: ``(n_samples, n_sampling_xs)``

    Grid datasets as described in :doc:`/tutorial/grids`:

    * ``sampling_xs``
    * ``scan_xs``

    Fitter output datasets and their shapes are explained in detail in :py:class:`~sigcorr.fitter.Fitter`:

    * ``b_loglikes``
    * ``b_params``
    * ``b_params_vars``
    * ``b_successes``
    * ``sb_loglikes``
    * ``sb_params``
    * ``sb_params_vars``
    * ``sb_successes``

The file also includes the attributes specifying the constant parameters of the model, which are
declared in the ``PARAMS`` field of the model class as described in the :doc:`/tutorial/model_definition`.

For better understanding, you can explore the examples of HDF5 files we used to build the documentation: :toy_data:`.`
