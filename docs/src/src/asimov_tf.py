import numpy as np
import h5py
import matplotlib.pyplot as plt

from sigcorr.tools.stats.utils import get_delta_sigs
from sigcorr.tools.stats.utils import sig2pval

from sigcorr.tools.stats.batch_stats import BatchStats2

from sigcorr.mapreduce.gp import gp_batch_mapreduce
from sigcorr.mapreduce.map_reducers import OverflowsCalc
from sigcorr.mapreduce.map_reducers import BatchStats1Reduce
from sigcorr.tools.utils import get_last_from_iter


bf_res = None
with h5py.File("../data/Asimov1D/hyy_tutorial.h5", "r") as fin:
    fields = ["b_loglikes", "sb_loglikes", "sb_params"]
    bf_res = {k: fin[k][:61, ...] for k in fields}
    xs = fin["scan_xs"][...].ravel()


sigs = get_delta_sigs(bf_res["b_loglikes"], bf_res["sb_loglikes"], bf_res["sb_params"][..., 0])

num_samples = sigs.shape[0]
sample_dim = sigs.shape[1]
bs = BatchStats2(num_samples, sample_dim)
bs.push(sigs)
cov = bs.get_corr(override_x=np.array([0.]))
cov_err = bs.get_corr_stat_err(override_x=np.array([0.]))


local_sig_grid = np.arange(0, 3.05, 0.1)  # i.e. significances between 0 and 3 with step 0.1

p_global_bs, _ = get_last_from_iter(
    gp_batch_mapreduce(cov,
                       1_000_000,  # num samples
                       2000,  # batch size
                       xs.shape,
                       OverflowsCalc(local_sig_grid),
                       BatchStats1Reduce())
)
p_global = p_global_bs.get_mean()
p_global_err = p_global_bs.get_stat_err()


local_p = sig2pval(local_sig_grid)
tf_gp = p_global/local_p
tf_gp_err = p_global_err/local_p

plt.errorbar(local_sig_grid, tf_gp, yerr=tf_gp_err, label="TF from GP toys", color="orange", marker=".", capsize=2)
plt.xlabel(r'$\mathrm{Z}_{local}$')
plt.ylabel("TF")
plt.title("Asimov trials factor")
plt.legend()


if __name__ == "__main__":
    plt.show()
