SigCorr
=======

`SigCorr <https://gitlab.com/sigcorr/sigcorr.git>`_  is a framework that implements a wide range of tools to conduct studies of the trials factor,
which is an important parameter in the statistical characterization of searches for new phenonema,
such as a hypothetical elementary particle or astrophysical source.
Such searches are commonly carried out for a range of possible signal hypotheses.
The statistical significance of the most significant observation (generally a localized excess of data in a broad background distribution)
must take into account the possibility that the background could have fluctuated anywhere in the search region, not just at the point of maximum interest.
Taking into account this so-called «look-elsewhere effect» is performed by correcting the corresponding p-value from its local to a global value.
The ratio of the former to the latter is known in the statistical literature as the trials factor.

There are several known ways to estimate the trials factor:

.. image:: img/tf_ways.dot.png
    :width: 60 %
    :align: center

|project| covers all these steps by providing useful tools that consistently operate together.

The project has a hybrid structure:

* From the perspective of fitting Monte Carlo simulations of the background process ("toys"), it is a framework. Users can implement
  their own statistical models following the guidelines and examples in the documentation.

* From the perspective of the analysis of fitted toys, however, SigCorr is a Swiss Army knife that assembles into
  one tool the knowledge from frequently cited as well as recent HEP papers that studied statistical hypothesis
  testing and the trials factor [1]_ [2]_ [3]_ [4]_.

It is worth mentioning, though, that the figure above is not exhaustive.
We believe there are more ways to approach the trials factor,
and some of them may also be possible to implement with the tools provided by |project|,
which are carefully described in the sections of the documentation listed below:

.. toctree::
   :maxdepth: 1
   :titlesonly:

   self
   installation
   design
   tutorial/tutorial
   api/api

Contribute
==========

* If you seek support or want to report a bug, please open a new `Issue <https://gitlab.com/sigcorr/sigcorr/-/issues>`_.
* If you want to contribute, feel free to fork `the project <https://gitlab.com/sigcorr/sigcorr.git>`_
  and open a Merge request.
* For developers willing to contribute, `README.md <https://gitlab.com/sigcorr/sigcorr>`_ contains more
  details on testing or building the documentation.


Cite this package
=================

.. code:: bibtex

    @article{Ananiev2023,
        doi = {10.21105/joss.04989},
        url = {https://doi.org/10.21105/joss.04989},
        year = {2023},
        publisher = {The Open Journal},
        volume = {8},
        number = {87},
        pages = {4989},
        author = {V. Ananiev and A. L. Read},
        title = {SigCorr: A Python package for studies of trials factors},
        journal = {Journal of Open Source Software}
    }


References
==========

.. [1] E. Gross and O. Vitells "Trial factors for the look elsewhere effect in high energy physics,"
       Eur. Phys. J. C 70, 525–530 (2010). https://doi.org/10.1140/epjc/s10052-010-1470-8
.. [2] O. Vitells and E. Gross, "Estimating the significance of a signal in a multi-dimensional search,"
       Astroparticle Physics, Volume 35, Issue 5, 2011, Pages 230-234, ISSN 0927-6505,
       https://doi.org/10.1016/j.astropartphys.2011.08.005
.. [3] G. Cowan and K. Cranmer and E. Gross et al. "Asymptotic formulae for likelihood-based tests of new physics,"
        Eur. Phys. J. C 71, 1554 (2011), https://doi.org/10.1140/epjc/s10052-011-1554-0
.. [4] V. Ananyev and A. L. Read, "Gaussian Process-based calculation of look-elsewhere trials factor,"
       JINST 18 (2023) P05041, https://doi.org/10.1088/1748-0221/18/05/P05041
