import h5py
import matplotlib.pyplot as plt
from sigcorr.tools.stats.utils import get_delta_sigs


bf_res = None
with h5py.File("../data/Asimov1D/hyy_tutorial.h5", "r") as fin:
    fields = ["b_loglikes", "sb_loglikes", "sb_params"]
    bf_res = {k: fin[k][:61, ...] for k in fields}
    xs = fin["scan_xs"][...].ravel()


sigs = get_delta_sigs(bf_res["b_loglikes"], bf_res["sb_loglikes"], bf_res["sb_params"][..., 0])
plt.plot(xs, sigs[10, ...], label=f"fluct. at {xs[10]} GeV")
plt.plot(xs, sigs[20, ...], label=f"fluct. at {xs[20]} GeV")
plt.plot(xs, sigs[40, ...], label=f"fluct. at {xs[40]} GeV")
plt.xlabel("m, GeV")
plt.ylabel("Z")
plt.title("Asimov significance curves")
plt.legend()


if __name__ == "__main__":
    plt.show()
