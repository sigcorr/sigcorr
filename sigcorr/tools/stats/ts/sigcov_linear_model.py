import numpy as np


def get_sigcov_for_linear_model(signals: np.ndarray, delta: np.ndarray) -> np.ndarray:
    """ Estimate covariance matrix of significances of a multidimensional linear model with Gaussian errors.

    Linear model has the form:

    .. math::

        f(\\vec{b}, \\mu) = \\vec{b_0} + \\langle b \\vert \\Delta + \\mu \\vec{s} .


    Parameters
    ----------
    signals : np.ndarray
        Array of injected signals of shape ``(corr_dim, xs)``.
        We estimate a correlation of their significances.
    delta : np.ndarray
        Coefficient matrix of the linear model.
        The calculation is vectorized, therefore the model expects ``delta`` to be an array of matrices
        of shape ``(num_samples, b_params, xs)``.

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.tools.stats.ts.sigcov_linear_model import get_sigcov_for_linear_model

        xs = np.array([1, 1.5, 2, 2.5, 3])
        signals = np.array([
            [1, 0.7, 0.5, 0, 0],
            [0.2, 1, 0.7, 0.5, 0],
            [0., 0.2, 1., 0.7, 0.5],
        ])  # shape (corr_dim=3, xs=5)
        delta = np.array([
            [xs,
             1/np.sqrt(xs)],
        ])  # shape (num_samples=1, b_params=2, xs=5)

        cov = get_sigcov_for_linear_model(signals, delta)  # shape (num_samples=1, corr_dim=3, corr_dim=3)
        cov[0]  # shape (corr_dim=3, corr_dim=3)

    Returns
    -------
        np.ndarray
            Array of covariace matrices of significances on the grid ``xs``
            estimated for a range of background parameter values.
            The resulting shape is ``(num_samples, xs, xs)``.
    """
    signals_norm = signals
    delta_norm = delta

    ddT_inv = np.linalg.inv(delta_norm @ delta_norm.transpose(0, 2, 1))
    kernel = np.eye(delta_norm.shape[2]) - (delta_norm.transpose(0, 2, 1) @ ddT_inv.transpose(0, 2, 1) @ delta_norm)

    nu1_hat = _solve_nu1_hat(delta_norm, signals_norm)
    denom = _get_denom(signals_norm, delta_norm, nu1_hat)

    signal_states = signals_norm/np.sqrt(denom)[..., np.newaxis]

    cov = signal_states @ kernel @ signal_states.transpose(0, 2, 1)
    return cov


def _solve_nu1_hat(delta_norm, signals_norm):
    #  num_samples x signal_loc x bg_params
    nu1_hat = np.linalg.solve(np.expand_dims(delta_norm @ delta_norm.transpose(0, 2, 1), axis=1),
                              -np.squeeze(
                                  np.expand_dims(delta_norm, axis=1) @ np.expand_dims(signals_norm, axis=(0, -1)),
                                  axis=-1
                               ))

    return nu1_hat


def _get_denom(signals_norm, delta_norm, nu1_hat):
    # signal_loc
    denom_s = np.squeeze(np.expand_dims(signals_norm, (-2)) @ np.expand_dims(signals_norm, -1), (-1, -2))

    # num_samples x signal_loc
    denom_nu1 = np.squeeze(
                       (np.expand_dims(nu1_hat, -2)  # num_samples x signal_loc x bg_params
                        @ np.broadcast_to(np.expand_dims(delta_norm, axis=1),
                                          nu1_hat.shape[:2] + delta_norm.shape[1:])  # num_samples x bg_params x xs
                        @ np.expand_dims(signals_norm, (0, -1)))  # signal_loc x xs
                       , (-1, -2))

    # num_samples x signal_loc
    denom = denom_s[np.newaxis, ...] + denom_nu1

    return denom
