"""
When the number of samples is known in advance but not all samples are
available at the moment (e.g. they don't fit into RAM), it is still possible
to estimate mean, variance, covariance and statistical errors
by processing the samples in batches.
"""

from typing import Optional
import numpy as np


class BatchStats1:
    r"""Batch statistics for mean value and errors.

    Instantiate this class with the total number of samples ``n_tot``, provide
    sample dimension ``sample_dim`` and then ``push`` batches of samples
    until entire dataset is processed.

    Then use methods ``get_mean``, ``get_var``, ``get_stat_err`` to extract
    summary statistics of the dataset.

    .. WARNING::

        Intermediate results are incorrect by default, because the partial sums ``self.x`` or
        ``self.xx`` are normalized by the total number of samples instead of the number of processed samples.

    Parameters
    ----------
        n_tot : int
            Total number of samples.
        sample_dim : tuple
            Dimension of one sample.

    Attributes
    ----------
        xx : np.ndarray
            Store mean squares: :math:`\frac{1}{n} \sum x_i^2`.
        x : np.ndarray
            Store mean: :math:`\frac{1}{n} \sum x_i`.
        n : int
            Total number of samples.

    Examples
    --------

        .. testcode::

            import numpy as np
            from sigcorr.tools.stats.batch_stats import BatchStats1

            n_samples = 3
            one_sample_shape = (2, )
            samples = np.array([[1, 2], [2, 3], [3., 4.]])

            bs = BatchStats1(n_samples, one_sample_shape)
            bs.push(samples[:1])  # push first batch (1 sample)
            bs.push(samples[1:])  # push second batch (2 samples)

            mean = bs.get_mean()
            # array([2., 3.])

            variance = bs.get_var()
            # array([3.33333333, 6.66666667])

            variance_zero_mean = bs.get_var(override_x=np.array([0, 0.]))
            # array([4.66666667, 9.66666667])

            variance_zero_mean2 = bs.get_var(override_x=np.array(0.))  # shortcut using broadcasting
            # array([4.66666667, 9.66666667])

            stat_error = bs.get_stat_err()
            # array([1.05409255, 1.49071198])
    """
    def __init__(self, n_tot: int, sample_dim: tuple):
        dim = sample_dim
        self.n: int = n_tot
        self.x: np.ndarray = np.zeros(dim)
        self.xx: np.ndarray = np.zeros(dim)

    def push(self, batch: np.ndarray):
        """Update state with batch of samples.

        Parameters
        ----------
            batch : np.ndarray
                Batch of samples shaped as ``(num_samples_in_batch, *sample_dim)``
        """
        self.x += batch.sum(axis=0)/self.n
        self.xx += (batch*batch).sum(axis=0)/self.n

    def get_mean(self) -> np.ndarray:
        """Get sample mean.

        Returns
        -------
            sample_mean : np.ndarray
                Return sample mean shaped as ``sample_dim``
        """
        return self.x

    def get_var(self, override_x: Optional[np.ndarray] = None) -> np.ndarray:
        r"""Get sample variance.

        .. math::

            var = \frac{1}{n} \sum (x_i - \bar{x})^2,

        where :math:`\bar{x}` is the mean that by default is estimated as sample mean. It
        can be overridden, however, with ``override_x`` parameter.

        Parameters
        ----------
            override_x : Optional[np.ndarray]
                If empty, mean is estimated as sample mean.
                When provided, compute variance assuming mean = ``override_x``.

        Returns
        -------
            sample_variance : np.ndarray
                Sample variance shaped as ``sample_dim``.
        """
        x = self.x if override_x is None else override_x
        variance = self.xx - x**2
        return variance

    def get_stat_err(self, override_x: Optional[np.array] = None) -> np.array:
        r"""Get statistical error for the sample mean.

        Assuming the errors are Gaussian:

        .. math::

            err = \sqrt{\frac{var}{n}} = \frac{std}{\sqrt{n}}

        Parameters
        ----------
            override_x : Optional[np.array]
                If empty, mean is estimated as sample mean.
                When provided, compute variance assuming mean = ``override_x``.

        Returns
        -------
            stat_error : np.ndarray
                Statistical error for the sample mean shaped as ``sample_dim``.
        """
        return np.sqrt(self.get_var(override_x=override_x)/self.n)


class BatchStats2:
    r"""Batch statistics for mean, covariance and errors.

    .. NOTE::

        Unlike :py:class:`~sigcorr.tools.stats.batch_stats.BatchStats1`,
        :py:class:`~sigcorr.tools.stats.batch_stats.BatchStats2` only supports flat samples with only 1 dimension.
        That's why ``sample_dim`` is ``int`` here instead of ``np.ndarray`` for ``BatchStats1``.

    Parameters
    ----------
        n_tot : int
            Total number of samples.
        sample_dim : int
            Size of one sample.

    Attributes
    ----------
        xyy : np.ndarray
            Store: :math:`\frac{1}{n} \sum_{\text{samples}} x^i \cdot (x^j)^2`
        x : np.ndarray
            Store: :math:`\frac{1}{n} \sum_{\text{samples}} x^i`
        xxyy : np.ndarray
            Store: :math:`\frac{1}{n} \sum_{\text{samples}} (x^i)^2 (x^j)^2`
        xy : np.ndarray
            Store: :math:`\frac{1}{n} \sum_{\text{samples}} x^i x^j`
        n : int
            Total number of samples

    Examples
    --------

        .. testcode::

            import numpy as np
            from sigcorr.tools.stats.batch_stats import BatchStats2

            n_samples = 3
            one_sample_shape = 2
            samples = np.array([[1, 2], [2, 3], [3., 4.]])

            bs = BatchStats2(n_samples, one_sample_shape)
            bs.push(samples[:1])  # push first batch (1 sample)
            bs.push(samples[1:])  # push second batch (2 samples)

            mean = bs.get_mean()
            # array([2., 3.])

            variance = bs.get_var()
            # array([3.33333333, 6.66666667])

            variance_zero_mean = bs.get_var(override_x=np.array([0, 0.]))
            # array([4.66666667, 9.66666667])

            variance_zero_mean2 = bs.get_var(override_x=np.array(0.))  # shortcut using broadcasting
            # array([4.66666667, 9.66666667])

            stat_error = bs.get_stat_err()
            # array([1.05409255, 1.49071198])

            covariance = bs.get_cov()
            # array([[0.66666667, 0.66666667],
            #        [0.66666667, 0.66666667]])

            covariance_variance = bs.get_cov_var()
            # array([[0.22222222, 0.22222222],
            #        [0.22222222, 0.22222222]])

            covariance_stat_error = bs.get_cov_stat_err()
            # array([[0.27216553, 0.27216553],
            #        [0.27216553, 0.27216553]])

            correlation = bs.get_corr()
            # array([[1., 1.],
            #        [1., 1.]])

            correlation_variance = bs.get_corr_var()
            # array([[0.5, 0.5],
            #        [0.5, 0.5]])

            correlation_stat_error = bs.get_corr_stat_err()
            # array([[0.40824829, 0.40824829],
            #        [0.40824829, 0.40824829]])

    """
    def __init__(self, n_tot: int, sample_dim: int):
        dim = sample_dim
        self.n: int = n_tot
        self.x: np.ndarray = np.zeros(dim)
        self.xy: np.ndarray = np.zeros((dim, dim))
        self.xyy: np.ndarray = np.zeros((dim, dim))
        self.xxyy: np.ndarray = np.zeros((dim, dim))

    def push(self, batch: np.ndarray):
        """Update state with batch of samples.

        Parameters
        ----------
            batch : np.ndarray
                Batch of samples shaped as ``(num_samples_in_batch, sample_dim)``
        """
        self.x += batch.sum(axis=0)/self.n
        self.xy += np.dot(batch.T, batch)/self.n
        sqr_batch = batch**2
        self.xyy += np.dot(batch.T, sqr_batch)/self.n
        self.xxyy += np.dot(sqr_batch.T, sqr_batch)/self.n

    def get_mean(self) -> np.ndarray:
        """Get sample mean.

        Returns
        -------
            sample_mean : np.ndarray
                Return sample mean shaped as ``sample_dim``
        """
        return self.x

    def get_var(self, override_x: Optional[np.ndarray] = None) -> np.ndarray:
        r"""Get sample variance.

        .. math::

            var = \frac{1}{n} \sum (x_i - \bar{x})^2,

        where :math:`\bar{x}` is the mean that by default is estimated as sample mean. It
        can be overridden, however, with ``override_x`` parameter.

        Parameters
        ----------
            override_x : Optional[np.ndarray]
                If empty, mean is estimated as sample mean.
                When provided, compute variance assuming mean = ``override_x``.

        Returns
        -------
            sample_variance : np.ndarray
                Sample variance shaped as ``sample_dim``.
        """
        return np.diagonal(self.get_cov(override_x=override_x))

    def get_stat_err(self, override_x: Optional[np.ndarray] = None) -> np.ndarray:
        r"""Get statistical error for the sample mean.

        Assuming the errors are Gaussian:

        .. math::

            err = \sqrt{\frac{var}{n}} = \frac{std}{\sqrt{n}}

        Parameters
        ----------
            override_x : Optional[np.array]
                If empty, mean is estimated as sample mean.
                When provided, compute variance assuming mean = ``override_x``.

        Returns
        -------
            stat_error : np.ndarray
                Statistical error for the sample mean shaped as ``sample_dim``.
        """
        return np.sqrt(self.get_var(override_x=override_x)/self.n)

    def get_cov(self, override_x: Optional[np.ndarray] = None) -> np.ndarray:
        r"""Get sample covariance.

        .. math::

            cov^{ij} = \frac{1}{n} \sum_{\text{samples}} (x^i - \bar{x}^i)(x^j - \bar{x}^j),

        where :math:`x^i` and :math:`x^j` are the i-th and j-th elements of the same sample, and
        :math:`\bar{x}` is the mean that by default is estimated as sample mean for each :math:`x^i`.
        The sample mean, however, can be overridden with ``override_x`` parameter.

        Parameters
        ----------
            override_x : Optional[np.ndarray]
                If empty, mean is estimated as sample mean.
                When provided, compute variance assuming mean = ``override_x``.

        Returns
        -------
            sample_covariance : np.ndarray
                Sample covariance matrix shaped as ``(sample_dim, sample_dim)``.
        """
        x = self.x if override_x is None else override_x
        x = x.reshape(1, -1)
        cov = self.xy - np.dot(x.T, x)
        return cov

    def get_cov_var(self, override_x: Optional[np.ndarray] = None) -> np.ndarray:
        r"""Get variance of the covariance.

        .. math::

            var(cov)^{ij} = \frac{1}{n} \sum_{\text{samples}}
                \left[(x_i - \bar{x}^i)(x_j - \bar{x}^j) - cov^{ij} \right]^2,

        where :math:`cov^{ij}` is the covariance from :py:func:`~sigcorr.tools.stats.batch_stats.BatchStats2.get_cov`.
        The sample mean :math:`\bar{x}` can be overridden with ``override_x`` parameter, which will also affect
        the ``get_cov`` computation.

        Parameters
        ----------
            override_x : Optional[np.ndarray]
                If empty, mean is estimated as sample mean.
                When provided, compute variance assuming mean = ``override_x``.

        Returns
        -------
            sample_variance_of_covariance : np.ndarray
                Sample variance of the covariance matrix shaped as ``(sample_dim, sample_dim)``.
        """
        x = self.x if override_x is None else override_x
        x = x.reshape(1, -1)
        sqr_x = x**2
        xx = np.diagonal(self.xy).reshape(1, -1)
        var = self.xxyy - self.xy**2 + np.dot(sqr_x.T, xx) + np.dot(xx.T, sqr_x) \
            - 2*x.T*self.xyy - 2*self.xyy.T*x \
            + 6*np.dot(x.T, x)*self.xy - 4*np.dot(sqr_x.T, sqr_x)
        return var

    def get_cov_stat_err(self, override_x: Optional[np.ndarray] = None) -> np.ndarray:
        r"""Get statistical error for the covariance matrix.

        Assuming the errors are Gaussian:

        .. math::

            err(cov)^{ij} = \sqrt{\frac{var(cov)^{ij}}{n}}

        Parameters
        ----------
            override_x : Optional[np.array]
                If empty, mean is estimated as sample mean.
                When provided, compute variances assuming mean = ``override_x``.

        Returns
        -------
            stat_error_of_covariance : np.ndarray
                Statistical error of the covariance matrix shaped as ``(sample_dim, sample_dim)``.
        """
        cov_var = self.get_cov_var(override_x=override_x)
        return np.sqrt(cov_var/self.n)

    def get_corr(self, override_x: Optional[np.ndarray] = None) -> np.ndarray:
        r"""Get sample correlation.

        .. math::

            corr^{ij} = \frac{cov^{ij}}{std_i \cdot std_j}.

        Apart from the normalization, the correlation matrix is built similarly to
        :py:func:`~sigcorr.tools.stats.batch_stats.BatchStats2.get_cov`.

        Parameters
        ----------
            override_x : Optional[np.ndarray]
                If empty, mean is estimated as sample mean.
                When provided, compute variance assuming mean = ``override_x``.

        Returns
        -------
            sample_correlation : np.ndarray
                Sample correlation matrix shaped as ``(sample_dim, sample_dim)``.
        """
        cov = self.get_cov(override_x=override_x)
        xx = np.diagonal(cov).reshape(1, -1)
        norm = np.dot(xx.T, xx)
        return cov/np.sqrt(norm)

    def get_corr_var(self, override_x=None):
        r"""Get variance of the correlation.

        .. math::

            var(corr)^{ij} = \frac{var(cov)^{ij}}{std_i \cdot std_j},

        where :math:`var(cov)^{ij}` is the variance of the covariance from
        :py:func:`~sigcorr.tools.stats.batch_stats.BatchStats2.get_cov_var`.
        The sample mean :math:`\bar{x}` can be overridden with ``override_x`` parameter, which will also affect
        the ``get_cov_var`` computation.

        Parameters
        ----------
            override_x : Optional[np.ndarray]
                If empty, mean is estimated as sample mean.
                When provided, compute variance assuming mean = ``override_x``.

        Returns
        -------
            sample_variance_of_correlation : np.ndarray
                Sample variance of the correlation matrix shaped as ``(sample_dim, sample_dim)``.
        """
        cov_var = self.get_cov_var(override_x=override_x)
        cov = self.get_cov(override_x=override_x)
        xx = np.diagonal(cov).reshape(1, -1)
        norm = np.dot(xx.T, xx)
        return cov_var/norm

    def get_corr_stat_err(self, override_x: Optional[np.ndarray] = None) -> np.ndarray:
        r"""Get statistical error for the correlation matrix.

        Assuming the errors are Gaussian:

        .. math::

            err(corr)^{ij} = \sqrt{\frac{var(corr)^{ij}}{n}}

        Parameters
        ----------
            override_x : Optional[np.array]
                If empty, mean is estimated as sample mean.
                When provided, compute variances assuming mean = ``override_x``.

        Returns
        -------
            stat_error_of_covariance : np.ndarray
                Statistical error of the correlation matrix shaped as ``(sample_dim, sample_dim)``.
        """
        corr_var = self.get_corr_var(override_x=override_x)
        return np.sqrt(corr_var/self.n)
