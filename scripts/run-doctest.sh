#!/bin/bash
set -x

ROOT_DIR=$(dirname "$0")/../
./$ROOT_DIR/docs/src/setup.sh
PYTHONPATH=$ROOT_DIR sphinx-build -d $ROOT_DIR/build/.doctrees -a -E -W -j4 -b doctest $ROOT_DIR/docs/src $ROOT_DIR/build/test
