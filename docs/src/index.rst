SigCorr
=======

`SigCorr <https://gitlab.cern.ch/sigcorr/sigcorr>`_ is a framework to study the trials factor. There are several known ways to estimate
the trials factor:

.. image:: img/tf_ways.dot.png
    :width: 60 %
    :align: center

|project| covers all these steps by providing useful utilities that consistently operate on defined data structures.
It is a framework, meaning, the user has to combine various utilities to build their own pipeline, that
might implement one of the approaches described above or even something new!


.. toctree::
   :maxdepth: 1
   :titlesonly:

   self
   installation
   design
   tutorial/tutorial
   api/api
