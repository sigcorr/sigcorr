from typing import TypeVar
from collections.abc import Iterable
from functools import partial
from concurrent.futures import ProcessPoolExecutor
import numpy as np
import scipy as sp
from scipy import optimize
import jax

from sigcorr.config import CFG


T = TypeVar("T")


def get_ridge_width(model, corr, xrange):
    widths = []
    with ProcessPoolExecutor(10) as pool:
        for fitres in pool.map(partial(_fit_ridge, model, corr, xrange), range(model.xs[xrange].shape[0])):
            widths.append(fitres[0])
    widths = np.array(widths)
    return widths


def _fit_ridge(model, corr, xrange, bin_i):
    corr_window = model.xs.shape[0]//6

    def ridge_model(xs, std):
        return sp.stats.norm(loc=model.xs[xrange][bin_i], scale=std).pdf(xs)/sp.stats.norm(loc=0, scale=std).pdf(0)

    corr_window_slice = slice(max(0, bin_i-corr_window), bin_i+corr_window)
    return sp.optimize.curve_fit(ridge_model, model.xs[xrange][corr_window_slice], corr[bin_i][corr_window_slice])[0]


def produce_batch_ranges(total, batchsize):
    n_full_batches = int(total // batchsize)
    for i in range(n_full_batches):
        yield slice(i*batchsize, (i+1)*batchsize)
    last_batch_size = int(total % batchsize)
    if last_batch_size > 0:
        yield slice(total-last_batch_size, total)


def get_last_from_iter(it: Iterable[T]) -> T:
    """Exhaust the iterator and return the last value.

    Parameters
    ----------
        it : Iterable[T]
            Input iterator.

    Returns
    -------
        last_elem : T
            Last element of the iterator.

    Examples
    --------

    .. testcode::

        from sigcorr.tools.utils import get_last_from_iter

        last_number = get_last_from_iter(range(10))  # returns 9
    """
    i = None
    for i in it:
        pass
    return i


def parse_grid_file(path: str) -> np.ndarray:
    """ Parse grid file into np array.

    We want to store grids in plain text, and this function defines the format.

    * Multidim arrays are stored flattened (``np.ravel``) in the file.
    * Original shape of the array should be stored in the header.

    Parameters
    ----------
        path : str
            Path to the input file with the grid.

    Returns
    -------
        grid : np.ndarray
            A numpy array with the grid read from file and reshaped according to the header.

    Examples
    --------

    Example input:

    .. code::

        ##shape -1,1
        100.0 101 102 103 104 105 106 ... 158 159 160.0

    Example code to read it:

    .. code::

        from sigcorr.tools.utils import parse_grid_file

        grid = parse_grid_file("path/to/file.dat")

    Resulting array:

    .. code::

        array([[100.],
               [101.],
               [102.],
               [103.],
               ...
               [157.],
               [158.],
               [159.],
               [160.]])

    .. seealso::

        :doc:`/tutorial/grids` in the tutorial.
    """
    shape = None
    str_shape = None
    with open(path, "r") as fin:
        for line in fin:
            if not line:
                continue
            if not line.startswith("#"):
                break
            if not line.startswith("##shape"):
                continue
            str_shape = line[len("##shape"):].replace(")", "").replace("(", "")
    if str_shape is not None:
        sep = "x" if "x" in str_shape else "," if "," in str_shape else "\t" if "\t" in str_shape else " "
        shape = tuple([int(i.strip()) for i in str_shape.split(sep)])
    arr = np.genfromtxt(path, dtype=np.float64)
    if shape is not None:
        arr = arr.reshape(shape)
    return arr


def shape_to_line_index(shape):
    yield from range(np.product(shape))


def jax_jit_method(method):
    """Mark a method with this decorator for Jax to JIT compile it.

    Parameters
    ----------
    method :
        The method to be JIT compiled by Jax

    Examples
    --------
    .. testcode::

        import jax.numpy as jnp
        from sigcorr.tools.utils import jax_jit_method

        class Model:
            @jax_jit_method
            def estimate(self, data):
                return jnp.mean(data)
    """
    return jax.jit(method, static_argnums=0)
