[![Documentation for the latest (dev) version](https://img.shields.io/badge/docs-dev-blue)](https://sigcorr.gitlab.io/sigcorr/latest)
[![License Apache 2](https://img.shields.io/badge/license-apache--2-green)](https://www.apache.org/licenses/LICENSE-2.0)

# SigCorr

$`\textcolor{red}{\text{Development of SigCorr was moved to \url{https://gitlab.com/sigcorr/sigcorr}. This repository is not maintained.}}`$ 

A framework to study the trials factor. See the [docs](https://sigcorr.gitlab.io/sigcorr/latest) (redirects to the maintained version hosted on Github) for installation details, usage examples and tutorials.

<div align="center">
    <img src="docs/src/img/tf_ways.dot.png" width="40%" />
</div>

## Installation

```
pip install git+https://:@gitlab.cern.ch:8443/sigcorr/sigcorr.git#egg=sigcorr
```

## Running

Source env vars to force JAX use float64 precision and disable its warnings:
```
source env.sh
```

```
sigcorr-run -o output_file.h5 -n 1000 GrossVitellsAsimov
```

Real world run (Hyy):

```
SIGCORR_FITTER_sbfit_batchsize=5 SIGCORR_FITTER_bfit_batchsize=300 SIGCORR_FITTER_sbfit_pool_size=250 SIGCORR_FITTER_bfit_pool_size=70 sigcorr-run -g sigcorr/grids/hyy-common.dat -o output/hyy-1m.h5 -f -n1000000 Hyy
```

Real world run (Gross and Vitells):

```
SIGCORR_FITTER_sbfit_batchsize=5 SIGCORR_FITTER_bfit_batchsize=300 SIGCORR_FITTER_sbfit_pool_size=250 SIGCORR_FITTER_bfit_pool_size=70 sigcorr-run -g sigcorr/grids/gross_vitells-sampling.dat -s sigcorr/grids/gross_vitells-scan.dat -o output/gross_vitells200k-1m.h5 -f -n1000000 GrossVitells
```

## Notebooks

Install dependencies

```
pip install -r notebooks/requirements.txt
```

Run jupyter lab:

```
jupyter lab notebooks/
```

## Build docs

For building docs, SigCorr should be installed (see the Installation above). Then run sphinx:

```
pip install sphinx matplotlib numpydoc sphinx-rtd-theme
./scripts/build-docs.sh
```

