"""
Examples
--------


Manually propagate a batch of curves through the pipeline that computes the
average number of up-crossings. This is what is happening under the hood in
:py:mod:`~sigcorr.mapreduce.file.h5_batch_mapreduce` and :py:mod:`~sigcorr.mapreduce.gp.gp_batch_mapreduce`,
but there the workload is distributed to many processes.

.. testcode::

    import numpy as np
    from sigcorr.tools.utils import get_last_from_iter
    from sigcorr.mapreduce.map_reducers import UpcrossCalc
    from sigcorr.mapreduce.map_reducers import BatchStats1Reduce

    xs = np.linspace(-np.pi/2, np.pi/2, 50)
    ys_batch = np.vstack([np.sin(xs),
                          2*np.sin(xs*2),
                          3*np.sin(xs*3)])
    batches = [ys_batch]  # one batch
    num_samples = sum(b.shape[0] for b in batches)  # should be 3 according to the example

    # build pipeline to compute up-crossings of level 2.5
    upcross_pipeline = UpcrossCalc(2.5)

    # mimic iterator that pushes batches through the pipeline
    processed_batches = (upcross_pipeline.process_batch(b) for b in batches)

    # compute average number of up-crossings
    bs_reducer = BatchStats1Reduce().reduce_batches(processed_batches, num_samples)  # result is the iterator

    # exhaust the iterator to get the result
    avg_upcross_bs, _ = get_last_from_iter(bs_reducer)

    # get mean from the BatchStats1 object
    avg_upcross_bs.get_mean()

It is possible to chain Calculators with :py:class:`~sigcorr.mapreduce.map_reducers.ChainCalc`:

.. code::

    from sigcorr.mapreduce.map_reducers import ChainCalc
    from sigcorr.mapreduce.map_reducers import MathCalc

    ...

    # square values of ys before computing the up-crossings
    upcross_pipeline = ChainCalc([MathCalc(np.square), UpcrossCalc(2.5)])

"""
from itertools import chain
from typing import List
from typing import Callable
from typing import Iterator
from typing import Iterable
from typing import Tuple
from typing import Optional

import numpy as np

from sigcorr.tools.stats.batch_stats import BatchStats1
from sigcorr.tools.stats.batch_stats import BatchStats2
from sigcorr.tools.euler_number import euler_number_along_zero_ax
from sigcorr.tools.upcross import zero_upcross_count
from sigcorr.tools.overflows import overflows_along_zero_ax
from sigcorr.tools.stats.utils import get_delta_sigs
from sigcorr.tools.stats.utils import get_delta_ts


class Calculator:
    """Abstract class for calculator to be used as a batch processor for :doc:`/api/mapreduce/batch_managers`.
    """

    def process_batch(self, *samples):
        """ Process one batch.

        There are no type restrictions here. Calculator can read any data and produce arbitrary types.
        The main restriction is that it should be consistent with the inputs provided
        and with the :py:mod:`~sigcorr.mapreduce.map_reducers.Reducer` being used.

        Parameters
        ----------
            *samples
                One batch may consist of several kinds of data. Calculator accepts different kinds of data as separate
                batches coming together as a tuple of ``samples``.
                For example length of the ``samples`` tuple should be compatible with number of ``fields``
                provided to :py:mod:`~sigcorr.mapreduce.file.h5_batch_mapreduce`.
        """
        raise NotImplementedError()


class ChainCalc(Calculator):
    """Chain Calculators to build a pipeline.

    Produce a :py:class:`~sigcorr.mapreduce.map_reducers.Calculator` from a sequence of Calculators.
    Each next Calculator takes input from the previous one.

    Parameters
    ----------
        calcs : List[Calculator]
            A sequence of calculators that consitute the pipeline.

            Currently, there is a limitation that only the first calculator can accept several kinds of data, rest should
            produce and accept a single kind output. There is no particular reason for this limitation, can be improved.
    """
    def __init__(self, calcs: List[Calculator]):
        self.calcs = calcs

    def process_batch(self, *samples):
        """
        Parameters
        ----------
            *samples
                any samples compatible with the first Calculator in the sequence ``calcs``.

        Returns
        -------
            processed_batch : Any
                Batch of samples propagated through the sequence of ``calcs`` from left to right.
        """
        res = self.calcs[0].process_batch(*samples)
        for calc in self.calcs[1:]:
            res = calc.process_batch(res)
        return res


class MathCalc(Calculator):
    """Construct Calculator from a Callable.

    This kind of Calculator is useful for example for calling ``np.reshape`` to transform data within the pipeline.

    Parameters
    ----------
        op : Callable
            A function that will be applied to the batch as a whole.
    """
    def __init__(self, op: Callable):
        self.op = op

    def process_batch(self, *samples):
        """
        Parameters
        ----------
            *samples
                Any input supported by the function ``op``. Should match the number of parameters.

        Returns
        -------
            Any:
                Processed batch with ``op``.
        """
        return self.op(*samples)


class Reducer:
    """Abstract class for Reducer that reduces batches of samples to a single value.
       To be used as a batch aggregator for for :doc:`/api/mapreduce/batch_managers`.
    """

    def reduce_batches(self, batch_iter: Iterator, n_samples: int):
        """ Aggregate stream of batches into a summary value.

        Reducer works like a coroutine, accepts iterator ``batch_iter`` and yields intermediate results after
        every processed batch. The last yielded result, after the input was exhausted, is the final
        result of the aggregation.

        There are no type restrictions here, similarly to the :py:class:`~sigcorr.mapreduce.map_reducers.Calculator`.
        The most important thing is that Reducer should be compatible with the output of the last Calculator in the
        pipeline.

        Parameters
        ----------
            batch_iter : Iterator
                Iterator over the processed batches
            n_samples : int
                total number of samples expected to come over all batches
        """
        yield from reduce_noop(batch_iter, n_samples)


def reduce_noop(batch_iter, n_samples):
    yield from batch_iter


class EulerNumberCalc(Calculator):
    """ Calculate Euler characteristic (number) at some level for a batch of the curves.

    Points of the object above the level ``c0`` are projected to this level, points below the level are rejected.
    Euler characteristic of the resulting object is then calculated.

    Parameters
    ----------
        c0 : float
            Level onto which to project the points from the above.
    """
    def __init__(self, c0: float):
        self.c0 = c0

    def process_batch(self, samples: np.ndarray) -> np.ndarray:
        """
        Parameters
        ----------
            samples : np.ndarray
                Euler characteristic is calculated along 0-axis of the input batch. The rest of the dimensions
                define the dimensionality of the object for which the Euler number is calculated.

        Returns
        -------
            np.ndarray
                1D array of the Euler characteristics for each object.

        """
        return euler_number_along_zero_ax(samples - self.c0).reshape(-1, 1)


class UpcrossCalc(Calculator):
    """ Calculate number of up-crossings of the level.

    Parameters
    ----------
        c0 : float
            Level above which up-crossings are computed.
    """
    def __init__(self, c0: float):
        self.c0 = c0

    def process_batch(self, samples: np.ndarray) -> np.ndarray:
        """
        Parameters
        ----------
            samples : np.ndarray
                2D array, where 0-axis enumerates curves, and 1-axis defines the curve on some grid.

        Returns
        -------
            np.ndarray
                1D array containing the numbers of up-crossings for each curve.
        """
        return zero_upcross_count(samples - self.c0).reshape(-1, 1)


class TestStatisticCalc(Calculator):
    """Calculate test statistic from the b and s+b likelihoods.
    """
    def process_batch(self, b_loglikes: np.ndarray, sb_loglikes: np.ndarray) -> np.ndarray:
        """
        Calculation is pointwise, therefore shapes of the inputs are not strictly defined, though they should match
        each other.

        Parameters
        ----------
            b_loglikes : np.ndarray
                Array of the b loglikelihoods.
            sb_loglikes : np.ndarray
                Array of the sb loglikelihoods. Can have more dimensions than ``b_loglikes``. The additional dimensions
                will account for the degrees of freedom not present under the background hypothesis.

        Returns
        -------
            np.ndarray
                Array of test statistics of the same shape as ``sb_loglikes``.
        """
        ts_batch = get_delta_ts(b_loglikes, sb_loglikes)
        return ts_batch


class SigsCalc(Calculator):
    """Calculate signed significances from the b and s+b likelihoods and fitted signal parameters.
    """
    def process_batch(self, b_loglikes: np.ndarray,
                      sb_loglikes: np.ndarray, sb_params: Optional[np.ndarray]) -> np.ndarray:
        """
        Calculation is pointwise, therefore shapes of the inputs are not strictly defined, though should match
        between each other.

        Parameters
        ----------
            b_loglikes : np.ndarray
                Array of the b loglikelihoods.
            sb_loglikes : np.ndarray
                Array of the sb loglikelihoods. Can have more dimensions than ``b_loglikes``. The additional dimensions
                will account for the degrees of freedom not present under the background hypothesis.
            sb_params : Optional[np.ndarray]
                Array of the fitted sb params. Sign of the first parameter will identify the sign of
                the calculated significance.

        Returns
        -------
            np.ndarray
                Array of significances of the same shape as ``sb_loglikes``.
        """
        sigs_batch = get_delta_sigs(b_loglikes, sb_loglikes, sb_params[..., 0])
        return sigs_batch


class OverflowsCalc(Calculator):
    """Check whether samples from the batch cross the thresholds from the provided set.
    """
    def __init__(self, local_sigs: np.array):
        """
        Parameters
        ----------
            local_sigs : np.array
                1D array of the thresholds.
        """
        self.local_sigs = local_sigs

    def process_batch(self, sig_curves: np.ndarray) -> np.ndarray:
        """
        Parameters
        ----------
            sig_curves : np.ndarray
                An array of samples aligned along 0-axis.

        Returns
        -------
            np.ndarray
                Array of shape ``(n_samples, num_thresholds)`` containing
                1 if the threshold is reached by the surface / curve / etc and 0 otherwise.
        """
        return overflows_along_zero_ax(sig_curves, self.local_sigs).astype(float)


class BatchStatsReduce(Reducer):
    BATCH_STATS_CLASS = None

    def reduce_batches(self, batch_iter, n_samples):
        num = 0
        batch_iter_active = iter(batch_iter)
        try:
            first_batch = next(batch_iter_active)
        except StopIteration:
            return
        input_sample_dim = np.product(first_batch.shape[1:])
        full_iter = chain([first_batch], batch_iter_active)
        covcalc = self.BATCH_STATS_CLASS(n_samples, input_sample_dim)  # pylint: disable=not-callable
        for batch_res in full_iter:
            covcalc.push(batch_res.reshape(-1, input_sample_dim))
            num += batch_res.shape[0]
            yield covcalc, num


class BatchStats1Reduce(BatchStatsReduce):
    """Prepare the :py:class:`~sigcorr.tools.stats.batch_stats.BatchStats1` object that contains information
       about the mean and variance between the input samples.
    """
    BATCH_STATS_CLASS = BatchStats1

    def reduce_batches(self, batch_iter: Iterator[np.ndarray], n_samples: int) -> Iterable[Tuple[BatchStats1, int]]:
        """
        Parameters
        ----------
            batch_iter : Iterator[np.ndarray]
                Generator of the 2D batches, where samples are aligned along 0-axis.
            n_samples : int
                Total number of input samples across all batches.

        Returns
        -------
            Iterable[Tuple[BatchStats1, int]]
                On each iteration yields a tuple containing the intermediate state of the
                :py:class:`~sigcorr.tools.stats.batch_stats.BatchStats1` object and
                current number of processed samples.
        """
        yield from super().reduce_batches(batch_iter, n_samples)


class BatchStats2Reduce(BatchStatsReduce):
    """Prepare the :py:class:`~sigcorr.tools.stats.batch_stats.BatchStats2` object that contains information
       about the mean and variance and covariance between the input samples.
    """
    BATCH_STATS_CLASS = BatchStats2

    def reduce_batches(self, batch_iter: Iterator[np.ndarray], n_samples: int) -> Iterable[Tuple[BatchStats2, int]]:
        """
        Parameters
        ----------
            batch_iter : Iterator[np.ndarray]
                Generator of the 2D batches, where samples are aligned along 0-axis.
            n_samples : int
                Total number of input samples across all batches.

        Returns
        -------
            Iterable[Tuple[BatchStats2, int]]
                On each iteration yields a tuple containing the intermediate state of the
                :py:class:`~sigcorr.tools.stats.batch_stats.BatchStats2` object and
                current number of processed samples.
        """
        yield from super().reduce_batches(batch_iter, n_samples)
