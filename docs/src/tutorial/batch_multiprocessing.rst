Batch multiprocessing
=====================

We assume that at this point you have a model defined according to :doc:`/tutorial/model_definition`
and :doc:`/tutorial/grids`, and data were sampled and fitted as described in Chapter :doc:`/tutorial/fitting`.
If you skipped the previous steps, just use :py:class:`~sigcorr.models.hyy.Hyy` for the model,
:src:`grids/hyy-common.dat` for the grid, and 3000 fitted samples from this model on this grid
stored in :toy_data:`3K1D/hyy_tutorial.h5`.


*Motivation:* The file containing 1M fits for the Hyy model has a size of 4 GB. Gaussian process toys contain only
information about significance, so they use less storage (~500MB per 1M toys). To approach practical
precision at high significances (:math:`~7\sigma`) one might require ~100M toys and a finer grid.
For many kinds of questions it is possible to avoid loading an entire dataset (~50GB) into RAM by
reading it in batches, making partial calculations in batches, and then aggregating the results.

Learn more about the MapReduce pipelines in the documentation: :doc:`/api/mapreduce/mapreduce`.


Estimating the covariance matrix: BatchStats multiprocessing
------------------------------------------------------------


We will use :py:func:`~sigcorr.mapreduce.file.h5_batch_mapreduce` to read samples from the file with likelihood fits.
We will provide names of the fields that will be extracted and fed to
:py:class:`~sigcorr.mapreduce.map_reducers.SigsCalc` for the significance curve calculation. Then, batches of the
significance curves will be aggregated with :py:func:`~sigcorr.mapreduce.map_reducers.BatchStats2Reduce`.

.. code::

    from sigcorr.mapreduce.file import h5_batch_mapreduce
    from sigcorr.mapreduce.map_reducers import SigsCalc
    from sigcorr.mapreduce.map_reducers import BatchStats2Reduce
    from sigcorr.tools.utils import get_last_from_iter

    bs, _ = get_last_from_iter(h5_batch_mapreduce("../data/hyy_tutorial.h5",
                                                  ["b_loglikes", "sb_loglikes", "sb_params"],
                                                  200,
                                                  SigsCalc(),
                                                  BatchStats2Reduce()))
    cov = bs.get_cov()
    cov_err = bs.get_cov_err()

Resulting plots from the batch multiprocessing that you can compare to the plots prepared manually in
the :ref:`previous section <tutorial/visualization_1d:Covariance matrix>`:

.. plot:: src/bs_covmat.py


Convergence of the average number of up-crossings when sampling the GP
----------------------------------------------------------------------


We will sample 100K significance curves from the covariance matrix
with :py:func:`~sigcorr.mapreduce.gp.gp_batch_mapreduce`, then,
with the help of :py:class:`~sigcorr.mapreduce.map_reducers.MathCalc`, we will turn them into test statistic curves.
The latter applies any function (in our case :py:func:`numpy.square`) to the input. Then we compute the average number
of up-crossings for the test statistic curves. For this we will chain two processors,
:py:class:`~sigcorr.mapreduce.map_reducers.MathCalc` and :py:class:`~sigcorr.mapreduce.map_reducers.UpcrossCalc`, using
:py:class:`~sigcorr.mapreduce.map_reducers.ChainCalc`.
During the calculaltion we will store the intermediate results from
:py:class:`~sigcorr.mapreduce.map_reducers.BatchStats1Reduce` to investigate the speed of convergence of the mean:

.. NOTE::
    Due to the specifics of the implementation of :py:mod:`~sigcorr.tools.stats.batch_stats`, intermediate values are
    still normalized to the total number of samples. We, therefore, rescale the numbers to get the correct value of
    the running mean.


.. code::

    from sigcorr.mapreduce.gp import gp_batch_mapreduce
    from sigcorr.mapreduce.map_reducers import ChainCalc
    from sigcorr.mapreduce.map_reducers import MathCalc
    from sigcorr.mapreduce.map_reducers import UpcrossCalc
    from sigcorr.mapreduce.map_reducers import BatchStats1Reduce

    ...
    nums = []
    avg_upcross = []
    pipeline = gp_batch_mapreduce(cov,
                                  100_000,  # num samples
                                  500,  # batch size
                                  xs.shape,  # sample shape
                                  ChainCalc([MathCalc(np.square), UpcrossCalc(1.)]),  # up-crossings at level 1.
                                  BatchStats1Reduce())
    for intermediate_bs, num_processed in pipeline:
        corrected_avg_upcross = intermediate_bs.get_mean()*intermediate_bs.n/num_processed
        avg_upcross.append(corrected_avg_upcross)
        nums.append(num_processed)

Here is the resulting convergence plot with a :math:`\pm 1\sigma` error band:

.. plot:: src/bs_running_upcross.py
