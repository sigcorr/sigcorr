Tutorial
========

In this tutorial we will set the Gross and Vitells upper bound for the Trials factor for a toy model
consisting of a search for a Gaussian signal peak on top of an exponential background distribution. This model, referred to hereafter as "Hyy", is inspired by searches for the Higgs boson by ATLAS and CMS in the :math:`H\rightarrow\gamma\gamma` channel.

The tutorial will guide you through the whole process
of using |project|, starting from the model definition and ending with the exploratory analysis
of the trials factor.

(requires the *development mode*) Defining the Hyy model and grids, and performing the likelihood scans:

.. toctree::
   :maxdepth: 1
   :titlesonly:

   /tutorial/model_definition.rst
   /tutorial/grids.rst
   /tutorial/fitting.rst

(*tools mode* is enough) Using likelihood scans to compute significance curves and covariance matrices,
to sample GP toys and to estimate the trials factor:

.. toctree::
   :maxdepth: 1
   :titlesonly:

   /tutorial/visualization_1d.rst
   /tutorial/batch_multiprocessing.rst
   /tutorial/visualization_2d.rst
   /tutorial/asimov_set_of_bg_samples.rst
