import numpy as np
import skimage as ski

from sigcorr.tools.upcross import zero_upcross_count


def euler_number_along_zero_ax(sigs: np.ndarray) -> np.ndarray:
    """Compute Euler number (Euler characteristic) of an array of images.

    Images are turned into black and white, where values ``> 0`` are black, and the rest is white.

    This function computes the Euler number of the image painted in black.

    In 1D case Euler number = number of up-crossings of the x-axis + (1 if the left edge point > 0),

    2D and 3D cases use skimage and loop over zero-axis with python loop.


    Parameters
    ----------
        sigs : np.ndarray
            Array of images, where the first (zero) axis enumerates images. Only allowed input dimensions: 2, 3, 4,
            that means 1D, 2D, and 3D images.

    Returns
    -------
        euler_numbers : np.ndarray
            Array of Euler numbers, one per image, with the shape of the 0-axis of the input.

    Examples
    --------

    Euler number for curves, 1D case:

    .. testcode::

        import numpy as np
        from sigcorr.tools.euler_number import euler_number_along_zero_ax

        xs = np.linspace(0, np.pi, 100)
        curves = np.stack([np.cos(xs), np.cos(2*xs), np.cos(3*xs), np.cos(4*xs)])
        euler_numbers = euler_number_along_zero_ax(curves)
        # [1 (left edge),
        #  2 (left edge + 1 up-cross),
        #  2 (left edge + 1 up-cross),
        #  3 (left edge + 2 up-cross)]

    Euler number for 2D images:

    .. testcode::

        import numpy as np
        from sigcorr.tools.euler_number import euler_number_along_zero_ax

        zeros = np.zeros((2, 2), dtype=int)

        one_peak = zeros.copy()
        one_peak[0, 1] = 1

        two_peaks = one_peak.copy()
        two_peaks[1, 0] = 1

        data = np.stack([zeros, one_peak, two_peaks])
        euler_numbers = euler_number_along_zero_ax(data)
        # [0, 1, 1]

    Note: In this example two peaks were counted as one beacause the connectivity is set to ``2``, which causes diagonal neighboring cells
    to be counted as neighbors. By the way, this is what Matlab uses by default.

    .. seealso::
        `Wiki: Euler characteristic <https://en.wikipedia.org/wiki/Euler_characteristic>`_

        :py:func:`skimage.measure.euler_number`,
        :py:mod:`sigcorr.tools.upcross`


    """
    image_dims = sigs.ndim - 1
    if image_dims > 3:
        raise NotImplementedError("Dimensions higher than 3 are not supported")
    if image_dims == 3:
        return euler_number_3d(sigs)
    if image_dims == 2:
        return euler_number_2d(sigs)
    if image_dims == 1:
        return euler_number_1d(sigs)
    raise NotImplementedError("0-dim euler num is not supported")


def euler_number_1d(sigs):
    return (sigs[..., 0] > 0) + zero_upcross_count(sigs)


def euler_number_2d(sigs):
    # connectivity chosen in consistency with MATLAB
    res_shape = sigs.shape[:-2]
    if not res_shape:
        return ski.measure.euler_number(sigs, connectivity=2)
    res = np.zeros(res_shape)
    for idx in np.ndindex(res_shape):
        res[idx] = ski.measure.euler_number(sigs[idx], connectivity=2)
    return res


def euler_number_3d(sigs):
    # connectivity left default to skimage
    res_shape = sigs.shape[:-3]
    if not res_shape:
        return ski.measure.euler_number(sigs)
    res = np.zeros(res_shape)
    for idx in np.ndindex(res_shape):
        res[idx] = ski.measure.euler_number(sigs[idx])
    return res
