import numpy as np
from sigcorr.models.model import (  # pylint: disable=unused-import  # noqa
    AbstractModel,
    LinearApproximable,
)
from sigcorr.tools.stats.ts.sigcov_linear_model import get_sigcov_for_linear_model


def get_linear_approx_sigcov_for_model(model: AbstractModel,  # is LinearApproximable
                                       xs: np.ndarray,
                                       b_params: np.ndarray,
                                       s_params: np.ndarray) -> np.ndarray:
    """ Covariance matrix of significances of discovery for a linear expansion of a statistical model

        The linear expansion of the ``model`` is built
        around background parameter values ``b_params``,
        and significances are estimated assuming Gaussian errors.

    Parameters
    ----------
    model : Intersection[AbstractModel, LinearApproximable]
        SigCorr model that implements :py:class:`~sigcorr.models.model.LinearApproximable` methods.
    xs : np.ndarray
        Grid on which the covariance should be evaluated.
    b_params : np.ndarray
        Background shape parameters around which to build the linear expansion.
        The calculation is vectorized with regard to this argument,
        therefore ``b_params`` is a dataset of parameters,
        1 set per line, and has shape ``(num_samples, num_b_params)``.
        (Most probably you need a dataset of 1 set containing the best background-only fit values)
    s_params : tuple[np.ndarray]
        Signal shape parameters. There are two things to take into account:

        1. In the linear approximation, the signal shape is proportional
           to the signal strength :math:`\\mu` (i.e. :math:`f = b + \\mu s`),
           which is assumed to be small, so signal parameters should be set to constants.
        2. The resulting matrix does not depend on the signal template normalization.

    Examples
    --------

    .. testcode::

        import numpy as np
        from sigcorr.models.gross_vitells import GrossVitells
        from sigcorr.linear_approx_sigcov import get_linear_approx_sigcov_for_model

        xs = np.arange(5, 120.1, 0.25)  # shape (461, )
        model = GrossVitells(xs).init()

        # signal has only normalization constant which does not affect the result
        s_params = (1, )

        # derivative of the background model is constant, so `b_params` don't really matter
        b_params = np.array([[1.]])  # num_samples = 1, num_b_params = 1

        # resulting shape: (1, 461, 461)
        cov = get_linear_approx_sigcov_for_model(model, xs, b_params, s_params)
        cov[0] # shape (461, 461)

    Returns
    -------
        np.ndarray
            Array of covariace matrices of significances on the grid ``xs``
            estimated for a range of background parameter values.
            The resulting shape is ``(num_samples, xs, xs)``.
    """
    new_model = model.copy()
    new_model.xs = xs  # regrid
    new_model.init()
    signals = new_model.expected_signal(xs[..., np.newaxis], *s_params)
    delta = new_model.expected_b_grad(*b_params.T)
    return get_sigcov_for_linear_model(signals, delta)
