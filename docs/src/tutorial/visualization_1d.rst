Visualization 1D
================

We assume that at this point you have a model defined according to :doc:`/tutorial/model_definition`
and :doc:`/tutorial/grids`, and toys were generated and fitted as described in the :doc:`/tutorial/fitting` chapter.
If you skipped the previous steps, just use the class :py:class:`~sigcorr.models.hyy.Hyy` for the model,
:src:`grids/hyy-common.dat` for the grid, and a sample of 3000 fitted toys from this model on this grid
stored in :toy_data:`3K1D/hyy_tutorial.h5`.


Test statistic and significance
-------------------------------


We start by reading some fields from the h5 file (yours or the stored one):

.. code::

    import h5py

    with h5py.File("../data/3K1D/hyy_tutorial.h5", "r") as fin:
        fields = ["b_loglikes", "sb_loglikes", "sb_params"]
        bf_res = {k: fin[k][:3000, ...] for k in fields}
        xs = fin["scan_xs"][...].ravel()

We read the fit results for the first 3000 toys into ``bf_res`` (for brute force toys results). We only have results for 3000 toys in the file,
however, it is a good practice to limit the number of toys until you are ready to produce final results.


.. NOTE::
    For the majority of use cases it is possible to read the results
    in batches and to accumulate only the information
    required to produce most of the other properties of interest (i.e. covariance matrices or averages). We will discuss
    the batched approach in the :doc:`next chapter </tutorial/batch_multiprocessing>`.

We use :py:func:`~sigcorr.tools.stats.utils.get_delta_ts` to convert values of log-likelihood into the test statistic and
then plot the test statistic as a function of ``xs``:

.. code::

    from sigcorr.tools.stats.utils import get_delta_ts

    ...
    sigs = get_delta_ts(bf_res["b_loglikes"], bf_res["sb_loglikes"])

.. plot:: src/tscurve.py

The test statistic we use is :math:`t = -2\ln\left[{\mathcal{L}(0,\hat{\hat\theta})\over \mathcal{L}(\hat\mu,\hat\theta)}\right]`,
i.e., the log-likelihood ratio of the background-only hypothesis to the signal+background hypothesis (with an unknown best-fit signal amplitude :math:`\hat\mu`),
with the nuisance parameters :math:`\theta` being optimized separately for the two hypotheses.

In the asymptotic regime, the square root of the test statistic with 1 d.o.f. is a significance, where the sign should be inferred from the
value of the fitted parameter (i.e. signal strength :math:`\hat{\mu}` or :math:`\mathrm{\hat{n}_{sig}}` in our case).
Let's plot a significance curve. For this we provide the values of the coefficients used to infer the sign as a third
argument:

.. code::

    from sigcorr.tools.stats.utils import get_delta_sigs

    ...
    sigs = get_delta_sigs(bf_res["b_loglikes"], bf_res["sb_loglikes"], bf_res["sb_params"][..., 0])

.. plot:: src/sigcurve.py

It is traditional to let :math:`t=0` for :math:`\hat\mu<0` (Eqn. 12 [1]_), but we need
the signed signal significance (both deficits and excesses) in order to treat the log-likelihood scan as a Gaussian Process (GP).


Covariance matrix
-----------------


We will use method :py:class:`~sigcorr.tools.stats.batch_stats.BatchStats2` to compute the covariance of the significance
curves we have produced:

.. code::

    from sigcorr.tools.stats.batch_stats import BatchStats2

    ...
    num_samples = sigs.shape[0]
    sample_dim = sigs.shape[1]
    bs = BatchStats2(num_samples, sample_dim)
    bs.push(sigs)
    cov = bs.get_cov()

:py:class:`~sigcorr.tools.stats.batch_stats.BatchStats2` needs to know number of samples and the dimension of a sample
in advance, howerer, there is no need to pass all the samples at once. This makes it possible to avoid keeping huge matrices
in RAM, and is a first step towards batched processing. The following would provide the same outcome:

.. code::

    from sigcorr.tools.stats.batch_stats import BatchStats2

    ...
    num_samples = sigs.shape[0]
    sample_dim = sigs.shape[1]
    bs = BatchStats2(num_samples, sample_dim)
    bs.push(sigs[:2000])
    bs.push(sigs[2000:])  # <-- we split one batch into two here
    cov = bs.get_cov()

It is also possible to extract the standard deviation and the statistical error for the samples,
the correlation matrix elements and
the covariance matrix elements (:math:`\mathrm{\Delta} \approx \frac{std}{\sqrt{num\ samples}}`).

.. code::

    ...
    sigs_std = bs.get_std()
    sigs_err = bs.get_stat_err()

    cov_std = bs.get_cov_std()
    cov_err = bs.get_cov_stat_err()

    corr_std = bs.get_corr_std()
    corr_err = bs.get_corr_stat_err()

The resulting covariance matrix and the error for each matrix element are shown here:

.. plot:: src/covmat.py


Gaussian process toys
---------------------


Let's assume that the correlation matrix of the local significances we estimated from 3000 MC toys is precise enough,
and let's sample 100 000 GP toy significance curves from this covariance matrix to estimate the trials factor:

.. code::

    from sigcorr.tools.stats.gp.sampling import draw_gp_svd

    ...
    gp_sigs = draw_gp_svd(corr, 100_000)


Here we use :py:func:`~sigcorr.tools.stats.gp.sampling.draw_gp_svd`, which is based on :py:func:`numpy.linalg.svd`
to compute the approximate square root of the covariance matrix. A more precise solution could use
the Cholesky decomposition (:py:func:`~sigcorr.tools.stats.gp.sampling.draw_gp_chol`),
however, due to numerical errors, the approximate covariance matrix turns out not always to be strictly positive definite.
In most cases SVD serves as a good way to filter away the numerical noise. If you would
prefer using your own square root implementation, you still can benefit from
:py:func:`~sigcorr.tools.stats.gp.sampling.draw_gp_from_sqrtcov` to sample the GP.

Example of sampled toy curves:

.. plot:: src/gp_sigcurves.py


Trials factor from GP toys
--------------------------


The trials factor is a ratio of the global p-value to the local p-value. While the local p-value can be calcualted
directly from the local significance (:py:func:`~sigcorr.tools.stats.utils.sig2pval`),
the global p-value requires additional computation. According to the definition,
we need to count how many significance curves exceed the predefined threshold corresponding to the local significance anywhere in the scanned region.

First, we define the grid of local significances for which we would like to determine the trials factor.
This is convenient for plotting. The grid will be:

.. code::

    local_sig_grid = np.arange(0, 3.05, 0.1)  # i.e. significances between 0 and 3 with step 0.1

Then we will use :py:func:`~sigcorr.tools.overflows.overflows_along_zero_ax` to produce an array
of ``True/False`` values for each significance curve to indicate whether the curve exceeded the corresponding threshold or not. We then calculate the global p-values from
the fractions of ``True`` values for each threshold:

.. code::

    from sigcorr.tools.overflows import overflows_along_zero_ax

    ...
    exceeds_or_not = overflows_along_zero_ax(gp_sigs, local_sig_grid)
    global_p = np.mean(exceeds_or_not, axis=0)

Then we compute the local p-values from the local significances and compute the trials factor:

.. code::

    from sigcorr.tools.stats.utils import sig2pval

    ...
    local_p = sig2pval(local_sig_grid)
    tf = global_p/local_p

Below is the plot of the trials factor as a function of the local significance computed from 100 000 GP toys compared
to the trials factor computed directly from the 3000 MC significance curves with the error bars estimated from the
Gaussian approximation:

.. plot:: src/tf.py


Gross and Vitells upper bound from up-crossings
-----------------------------------------------


There are 2 alternative ways to set the upper bound from up-crossings:

* For the significance curve, compute the average Euler characteristic of the set of points above the threshold
  (:py:func:`~sigcorr.tools.euler_number.euler_number_along_zero_ax`),
  and propagate the latter to other significance levels
  (:py:func:`~sigcorr.tools.stats.gp.euler_number.GPEulerNumberPropagator`).
  According to Gross and Vitells [2]_, the average Euler characteristic of a set of points above some
  threshold serves as an upper bound for the global p-value for the corresponding local significance.

    .. code::

        from sigcorr.tools.euler_number import euler_number_along_zero_ax
        from sigcorr.tools.stats.gp.euler_number import GPEulerNumberPropagator

        ...
        ref_threshold = 1.
        euler_numbers = euler_number_along_zero_ax(sigs - ref_threshold)
        avg_euler_number = np.mean(euler_numbers, axis=0)
        propagator = GPEulerNumberPropagator(np.array([ref_threshold]), avg_euler_number)
        gv_upper_bound_from_samples = propagator.calc(local_sig_grid)

  Notice how we subtracted the ``ref_threshold`` from ``sigs`` before passing it to
  :py:func:`~sigcorr.tools.euler_number.euler_number_along_zero_ax`. The latter computes the average Euler characteristic
  of a black-and-white image where positive values are set to ``1`` and negative to ``0``. We needed to make sure
  the points above the threshold appear as ``>0`` and the points below the threshold as ``<0``.


* From the covariance matrix, we can directly estimate the
  average number of up-crossings at any level for the Gaussian process approximation of the significance curve
  (:py:func:`~sigcorr.tools.stats.gp.upcross.gp_upcross_at_level`). Then we estimate the average Euler characteristic for the
  Gaussian process with (:py:func:`~sigcorr.tools.stats.gp.euler_number.avg_gp_euler_number`):

    .. code::

        from sigcorr.tools.stats.gp.upcross import gp_upcross_at_level
        from sigcorr.tools.stats.gp.euler_number import avg_gp_euler_number

        ...
        avg_upcross = np.array([gp_upcross_at_level(xs, cov, sig) for sig in local_sig_grid])
        avg_euler_numbers = avg_gp_euler_number(local_sig_grid, avg_upcross)
        gv_upper_bound_from_cov = avg_euler_numbers

Here is a comparison of the upper bounds of the trials factor to the GP and brute force approaches:

.. plot:: src/tf_gv_upper.py


References
----------

.. [1] G. Cowan and K. Cranmer and E. Gross et al. "Asymptotic formulae for likelihood-based tests of new physics,"
        Eur. Phys. J. C 71, 1554 (2011), https://doi.org/10.1140/epjc/s10052-011-1554-0
.. [2] E. Gross and O. Vitells "Trial factors for the look elsewhere effect in high energy physics,"
       Eur. Phys. J. C 70, 525–530 (2010). https://doi.org/10.1140/epjc/s10052-010-1470-8