import os
import json
from dataclasses import (
    dataclass,
    field,
    fields,
)
from typing import Optional
import yaml


@dataclass
class AbstractConfig:
    pass


@dataclass
class FitterConfig(AbstractConfig):
    sbfit_pool_size: int = 4
    bfit_pool_size: int = 4
    sbfit_batchsize: int = 5
    bfit_batchsize: int = 100
    optim_options: Optional[dict] = field(default_factory=lambda: {"xtol": 1E-10})
    optim_method: str = "Newton-CG"


@dataclass
class Config(AbstractConfig):
    FITTER: FitterConfig = field(default_factory=FitterConfig)


def load_config_from_file(cfg, filename):
    with open(filename, "r", encoding="utf-8") as fin:
        try:
            parsed = json.load(fin)
            return
        except json.JSONDecodeError:
            fin.seek(0)
            parsed = yaml.safe_load(fin)
    load_config_from_json(cfg, parsed)


def load_config_from_json(cfg, data):
    for key, val in data.items():
        config_obj = getattr(cfg, key)
        if isinstance(config_obj, AbstractConfig):
            if isinstance(val, dict):
                load_config_from_json(config_obj, val)
            else:
                raise ValueError("trying to assign value to config namespace")
        try:
            parsed_val = json.loads(val)
        except (json.JSONDecodeError, TypeError):
            parsed_val = val
        setattr(cfg, key, parsed_val)


def load_config_from_env(cfg):
    for key, val in os.environ.items():
        if key == "SIGCORR_CFG":
            continue
        if not key.startswith("SIGCORR_"):
            continue
        cfg_object, key = find_cfgobject_by_key(cfg, key[len("SIGCORR_"):])
        parsed_val = json.loads(val)
        setattr(cfg_object, key, parsed_val)


def find_cfgobject_by_key(cfg, key):
    res = cfg
    parts = key.split("_")
    for i, prefix in enumerate(parts):
        try:
            newres = getattr(res, prefix)
        except AttributeError:
            break
        if not isinstance(newres, AbstractConfig):
            break
        res = newres
    else:
        raise KeyError("Config key doesn't mention the field")
    return res, "_".join(parts[i:])


def dump_config(cfg):
    res = {}
    for fld in fields(cfg):
        field_name = fld.name
        field_val = getattr(cfg, field_name)
        if isinstance(field_val, AbstractConfig):
            res[field_name] = dump_config(field_val)
        else:
            res[field_name] = field_val if not isinstance(field_val, (dict, list)) else json.dumps(field_val)
    return res


CFG = Config()
